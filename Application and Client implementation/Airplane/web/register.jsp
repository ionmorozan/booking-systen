<%--
    Document   : ex
    Created on : Oct 19, 2011, 5:28:12 PM
    Author     : John
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <form action="Register" method="post">
            <div id="wrapper">
                <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
                <div id="booking">
                    <h2>Enter your username and password</h2>

                    <h3 class="blue_2">Choose your account type!</h3>

                    <!--radio button to select the account type -->
                    <div class="jtype">
                        <label for="Company">Company</label>
                        <input name="group" id="Company" type="radio" value="Company" onclick="company();"/>
                        &nbsp; &nbsp;
                        <label for="User">User</label>
                        <input name="group" id="User" type="radio" value="User" checked="yes" onclick="user()"/>
                    </div>
                </div>
                <!-- end booking -->
                <div id="nav">
                    <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <li><a href="contact.jsp">Contact</a></li>
                    </ul>
                </div>

                <div id="main">
                    <img src="images/register_logo.jpg" width="447" height="125" alt="login" class="block" />
                    <div class="clear"></div>
                </div>

                <div align="center" id="userTable" style="display:block;visibility:visible;">
                    <table CLASS="boldtable" border="0" >
                        <tr>
                            <td> First Name*</td>
                            <td><input name="firstName" type="text" size="30"/></td>
                        </tr><tr>
                            <td> Last Name*</td>
                            <td><input name="lastName" type="text" size="30" /></td>
                        </tr><tr>
                            <td> E-mail Address*</td>
                            <td><input type="text" name="email" size="30" autocomplete="off"/></td>
                        </tr><tr>
                            <td> Username*</td>
                            <td><input type="text" name="username" size="30" autocomplete="off" /></td>
                        </tr><tr>
                            <td> Password*</td>
                            <td><input type="password" name="password" size="30" autocomplete="off" /></td>
                        </tr><tr>
                            <td> Password again*</td>
                            <td><input type="password" name="passwordAgain" size="30" autocomplete="off" /></td>
                        </tr><tr>
                            <td> Phone*</td>
                            <td><input name="phone" type="text" size="30" /></td>
                        </tr><tr>
                            <td><strong><font size="3">Address(Optional)</font></strong></td>
                        </tr><tr>
                            <td>Street/No</td>
                            <td><input name="street" type="text" size="30" /></td>
                        </tr><tr>
                            <td>City</td>
                            <td><input name="city" type="text" size="30" /></td>
                        </tr><tr>
                            <td>Department</td>
                            <td><input name="department" type="text" size="30" /></td>
                        </tr><tr>
                            <td><INPUT type="reset" name="reset" /></td>
                            <td align="right"><input type="submit" name="submit" value="Submit"></td>
                        </tr>
                        <tr>
                            <td><font size="2"><br/>* Required fields!</font></td>
                        </tr>
                        ${fillAllFields}
                        ${userAlreadyExists}
                        ${wrongPhoneFormat}
                        ${passDontMatch}
                    </table>
                    <div class="clear"></div>
                </div>

                <div align="center" id="companyTable" style="display:none;visibility:hidden;">
                    <table CLASS="boldtable" border="0" >
                        <tr>
                            <td> Company Name*</td>
                            <td><input name="nameCom" type="text" size="30"/></td>
                        </tr><tr>
                            <td> Company Registration Number*</td>
                            <td><input type="text" name="regNumber" size="30" /></td>
                        </tr><tr>
                            <td> Registration Authority*</td>
                            <td><input type="text" name="regAuth" size="30" /></td>
                        </tr><tr>
                            <td> E-mail Address*</td>
                            <td><input type="text" name="emailCom" size="30" autocomplete="off" /></td>
                        </tr><tr>
                            <td> Username*</td>
                            <td><input type="text" name="usernameCom" size="30" autocomplete="off" /></td>
                        </tr><tr>
                            <td> Password*</td>
                            <td><input type="password" name="passwordCom" size="30" autocomplete="off"/></td>
                        </tr><tr>
                            <td> Password again*</td>
                            <td><input type="password" name="passwordAgainCom" size="30" autocomplete="off"/></td>
                        </tr><tr>
                            <td> Phone*</td>
                            <td><input name="phoneCom" type="text" size="30" /></td>
                        </tr><tr>
                            <td><strong><font size="3">Company Address(Optional)</font></strong></td>
                        </tr><tr>
                            <td>Street/No</td>
                            <td><input name="streetCom" type="text" size="30" /></td>
                        </tr><tr>
                            <td>City</td>
                            <td><input name="cityCom" type="text" size="30" /></td>
                        </tr><tr>
                            <td>Department</td>
                            <td><input name="departmentCom" type="text" size="30" /></td>
                        </tr><tr>
                            <td><INPUT type="reset" name="reset" /></td>
                            <td align="right"><input type="submit" name="submit" value="Submit"></td>
                        </tr>
                        <tr>
                            <td><font size="2"><br/>* Required fields!</font></td>
                        </tr>
                        ${fillAllFields}
                        ${userAlreadyExists}
                        ${wrongRegNumberFormat}
                        ${wrongPhoneFormat}
                        ${passDontMatch}
                    </table>
                    <div class="clear"></div>
                </div>

                <script>
                    /*function called when radio button user is checked, and
                     *the user table is displayed*/
                    function user()
                    {
                        document.getElementById('companyTable');
                        companyTable.style.display = 'none';
                        companyTable.style.visibility = 'hidden';

                        document.getElementById('userTable');
                        userTable.style.display = 'block';
                        userTable.style.visibility = 'visible';

                    }
                    /*function called when radio button company is checked, and
                     *the user table is displayed*/
                    function company()
                    {
                        document.getElementById('userTable');
                        userTable.style.display = 'none';
                        userTable.style.visibility = 'hidden';
                        
                        document.getElementById('companyTable');
                        companyTable.style.display = 'block';
                        companyTable.style.visibility = 'visible';
                    }
                </script>
                <!-- end main -->
                <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
                <!-- end footer -->
            </div>
        </form>
    </body>
</html>
