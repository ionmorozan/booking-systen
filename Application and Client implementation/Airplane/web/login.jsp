<%-- 
    Document   : ex
    Created on : Oct 19, 2011, 5:28:12 PM
    Author     : John
--%>

<%@page import="java.util.List"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <%
            
                session = request.getSession(false);
                if (!session.isNew())
                    {
                    session.removeAttribute("username");
                    session.removeAttribute("userType");
                    }
        %>
        <form action="Login" method="post">
            <div id="wrapper">
                <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
                <div id="booking">
                    <h2>Enter your username and password</h2>

                    <h3 class="blue_2">Choose your account type!</h3>

                    <!--radio button to select the account type -->
                    <div class="jtype">
                        <label for="Company">Company</label>
                        <input name="group" id="Company" type="radio" value="Company" />
                        &nbsp; &nbsp;
                        <label for="User">User</label>
                        <input name="group" id="User" type="radio" value="User" checked="yes"  />
                    </div>
                </div>
                <!-- end booking -->
                <div id="nav">
                    <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <li><a href="contact.jsp">Contact</a></li>
                    </ul>
                </div>

                <div id="main">
                    <img src="images/login.jpg" width="447" height="125" alt="login" class="block" />
                    <div class="clear"></div>
                </div>

                <div align="center">
                    <table summary="" cellspacing="0" cellpadding="5" border="0">
                        <tr>
                            <th>Username</th>
                            <td><input name="username" type="text" value="" class="text" autocomplete="off"/></td>
                        </tr>
                        <tr>
                            <th>Password</th>
                            <td><input name="password" type="password" class="text" autocomplete="off"/></td>
                        </tr>
                        ${errorMessage}
                        <tr>
                            <td><input type="submit" value="Login" class="submit" /></td>
                        </tr>
                    </table>
                    <div class="clear"></div>
                </div>

                <!-- end main -->
                <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
                <!-- end footer -->
            </div>
        </form>
    </body>
</html>