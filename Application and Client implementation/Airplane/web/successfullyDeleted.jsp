<%--
    Document   : successfullyDeleted
    Created on : 22.10.2011, 21:16:28
    Author     : Andreea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>

    <!-- Copyright
        Copyright 2009 Itamar Arjuan
	jsDatePick is distributed under the terms of the GNU General Public License.
    -->

    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <%
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            String userType = (String) session.getAttribute("userType");
                            if(username==null) username="Guest";
                            if(userType == null) userType="";
                            if (username.equals("Guest")) {
                        %>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <%      } else {
                        %>
                        <li><a href="login.jsp">Logout</a></li>
                        <li><a href="profile.jsp">Profile</a></li>
                        <%                          }
                            if (userType.equals("Company"))
                                {
                        %>
                        <li><a href="company.jsp">Panel</a></li>
                        <%
                            } else if (userType.equals("Admin")){
                        %>
                        <li><a href="adminPanel.jsp">Panel</a></li>
                        <%
                            } else {
                        %>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <%
                            }
                        %>
                        <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/admin.jpg" width="447" height="125" alt="adminPanel" class="block" />
                <div class="clear"></div>
            </div>

            <br>
            <div align="center"><strong>
                    <br>
                    <font size="4" color="green">Your account was successfully deleted!
                    </font>
                    <br>
                </strong></div>
            <br>
            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>
