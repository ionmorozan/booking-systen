<%-- 
    Document   : adminPanel
    Created on : 22.10.2011, 21:50:26
    Author     : Andreea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>


    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <br><br>
                <div class="jtype" align="center">
                    <form action="AdminDeleteAccount" method="post">
                        <div align="center">
                            <table summary="" cellspacing="0" cellpadding="5" border="0">
                                <tr>
                                    <td>Username</td>
                                    <td><input name="usernameDel" type="text" value="" class="text" autocomplete="off"/></td>
                                </tr>
                                ${errorMessage}
                                ${accountDeleted}
                                <tr>
                                    <td colspan="2"><input type="submit" value="Delete Account" class="submit" name="deleteAccount" /></td>
                                </tr>
                            </table>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                        session = request.getSession(false);
                        String userType = (String) session.getAttribute("userType");
                        if(userType==null) userType="";
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          
                        if (userType.equals("Company"))
                            {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%
                        } else if (userType.equals("Admin")){
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%
                        } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%
                        }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/admin.gif" width="447" height="125" alt="adminPanel" class="block" />
                <div class="clear"></div>
            </div>

            
            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>
