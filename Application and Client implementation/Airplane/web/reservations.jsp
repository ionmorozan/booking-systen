<%-- 
    Document   : reservations
    Created on : Nov 28, 2011, 12:32:20 PM
    Author     : Ion Morozan & Sandu Andreea
--%>

<%@page import="classes.HotelReservation"%>
<%@page import="java.util.List"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            session = request.getSession(false);
                            String username = (String) session.getAttribute("username");
                            if (username == null) {
                                username = "Guest";
                            }
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                <div class="jtype" align="center">
                    <form action="UpdateReservation" method="post">
                        <div align="center">
                            <table summary="" cellspacing="0" cellpadding="5" border="0">
                                <tr>
                                    <td>Reservation Id</td>
                                    <td><input name="reservationid" type="text" value="" class="text" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <th>Check-in</th>
                                    <td><input type=date name=adate value="" size="9"></td>
                                </tr>
                                <tr>
                                    <th>Check-out</th>
                                    <td><input type=date name=rdate value="" size="9"></td>
                                </tr>
                                <tr>
                                    <td ><input type="submit" value="Delete" class="submit" name="deleteReservation" /></td>
                                    <td ><input type="submit" value="Update" class="submit" name="updateReservation" /></td>
                                </tr>
                                ${errorMessage}
                            </table>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                                session = request.getSession(false);
                                String userType = (String) session.getAttribute("userType");
                                if (userType == null) {
                                    userType = "";
                                }
                                if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                                if (userType.equals("Company")) {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%                                                    } else if (userType.equals("Admin")) {
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%                                                    } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%                                                    }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/booking.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>

            <div align="center" style="padding-top: 25px">

                <%
                            /* instance of database */
                            DAO dao = DAO.getInstance();
                            if (username.compareTo("Guest") != 0) {

                %>
                <%
                                                List<HotelReservation> UserReservations = dao.getHotelReservation(username);
                                                if (UserReservations.size() != 0) {
                %>
                <h2><font size="3" color="green">Your reservations:</font></h2><br>
                <%
                                                                    for (int i = 0; i < UserReservations.size(); i++) {

                %>
                <div align="left" style="padding-top: 15px">
                    <table summary="" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th><font size="3" color="brown"><%=UserReservations.get(i).getCity()%></font></th>
                        </tr>
                        <tr>
                            <th>Reservation ID</th>
                            <td><input type=text name="reservation_id" value="<%=UserReservations.get(i).getId()%>" readonly="readonly" size="5"></td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td><input type=text name="name" value="<%=UserReservations.get(i).getName()%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>Check-in</th>
                            <td><input type=date name=adate value="<%=UserReservations.get(i).getStart().substring(0, 10)%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>Check-out</th>
                            <td><input type=date name=ddate value="<%=UserReservations.get(i).getEnd().substring(0, 10)%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>Room Size</th>
                            <td><input type=text name="room_size" value="<%=UserReservations.get(i).getNrOfPers()%>" readonly="readonly" size="1"></td>
                        </tr>
                        <tr>
                            <th>Description</th>
                            <td><input type=text name="descr" value="<%=UserReservations.get(i).getDesc()%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>
                                <input name="email" type="text" value="<%=UserReservations.get(i).getEmail()%>" class="text" readonly="readonly"/>
                            </td>
                        </tr>
                    </table>
                </div>


                <%
                                                                    }
                                                                } else {
                %>
                <div align="center" style="padding-top: 25px">
                    <h2><font size="3" color="red">You don't have any reservation!</font></h2>
                    <p><a href="index.jsp">Go Back!</a></p>
                </div>

                <%                                                        }

                %>
                <%
                                            } else {
                %>
                <p><font color="red" size="3"><strong>Note!</strong></font>
                    <font size="2">Update a reservation is available only for <a href="register.jsp">registered</a> user!</font>
                </p>
                <%                                }
                %>

                <div class="clear"></div>
            </div>

            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>
