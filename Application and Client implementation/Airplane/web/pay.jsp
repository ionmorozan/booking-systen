
<%@page import="classes.DAO"%>
<%--
    Document   : book
    Created on : Oct 22, 2011, 3:17:07 PM
    Author     : John
--%>
<%@ page import="java.util.Random;" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            session = request.getSession(false);
                            String username = (String) session.getAttribute("username");
                            if (username == null) {
                                username = "Guest";
                            }
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                <br><br>
                <div class="jtype" style="padding-left:22px" style="padding-top:100px">
                    <img align ="center" src="images/card.jpg" width="200" height="120" alt="card"/>
                </div>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                                session = request.getSession(false);
                                String userType = (String) session.getAttribute("userType");
                                String ddate = (String) session.getAttribute("ddate");
                                String adate = (String) session.getAttribute("adate");

                                if (userType == null) {
                                    userType = "";
                                }
                                if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                                if (userType.equals("Company")) {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%                                    } else if (userType.equals("Admin")) {
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%                                    } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%                                    }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/payment.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>

            <div align="center" style="padding-top: 25px">
                <h3><font size="3"> Payment successful! </font></h3>
                <h3><font size="3">Your ticket will be send by email!</font></h3>
                <%
                            Random checkinNr = new Random();
                            int confirmationNumber = Math.abs(checkinNr.nextInt());
                            DAO dao = DAO.getInstance();
                            dao.setConfirmationNumber(username, confirmationNumber);
                %>
                <h3><font size="3" color="red">Check-in code: <%=confirmationNumber%></font></h3>
                <div class="clear"></div>
            </div>

            <div align="center" style="padding-top: 25px">
                ${errorMessage}
                <h2><font size="2" color="green">Do you want to book a hotel room for your stay?</font></h2>
                <form action="SearchHotel" method="post">
                    <table summary="" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>Check-in</th>
                            <td><input type=date name=adate value="<%=ddate%>"></td>
                        </tr>
                        <tr>
                            <th>Check-out</th>
                            <td><input type=date name=ddate value="<%=adate%>"></td>
                        </tr>
                        <tr>
                            <th>Room Size</th>
                            <td>
                                <select name="room_size" tabindex="4" class="pass">
                                    <option value="1" selected="selected">1</option>
                                    <option value="2">2</option>
                                    <option value="3" >3</option>
                                </select>
                                <input type="submit" value="Check Availability!" class="submit" name="availability" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <br>
            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>