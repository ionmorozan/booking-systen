<%-- 
    Document   : company
    Created on : 22.10.2011, 15:36:28
    Author     : Andreea
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>

    <!-- Copyright
        Copyright 2009 Itamar Arjuan
	jsDatePick is distributed under the terms of the GNU General Public License.
    -->
    <link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
    <script type="text/javascript" src="jsDatePick.min.1.3.js"></script>
    <script type="text/javascript">
        window.onload = function(){
            new JsDatePick({
                useMode:2,
                target:"returnDate",
                dateFormat:"%d-%M-%Y"
            });
            new JsDatePick({
                useMode:2,
                target:"departureDate",
                dateFormat:"%d-%M-%Y"
            });
        };
    </script>

    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <br><br>
                <div class="jtype" align="center">
                    <form action="DeleteAccount" method="post">
                        <div align="center">
                            <table summary="" cellspacing="0" cellpadding="5" border="0">
                                <tr>
                                    <td>Username</td>
                                    <td><input name="usernameDel" type="text" value="" class="text" autocomplete="off"/></td>
                                </tr>
                                <tr>
                                    <td>Password</td>
                                    <td><input name="passwordDel" type="password" class="text" autocomplete="off"/></td>
                                </tr>
                                ${errorMessage}
                                <tr>
                                    <td colspan="2"><input type="submit" value="Deactivate Account" class="submit" name="deleteAccount" /></td>
                                </tr>
                            </table>
                            <div class="clear"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                        session = request.getSession(false);
                        String username=(String) session.getAttribute("username");
                        String userType = (String) session.getAttribute("userType");
                        if(username==null) username="Guest";
                        if(userType == null) userType="";
                        if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                        if (userType.equals("Company"))
                            {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%
                        } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%
                        }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/admin.gif" width="447" height="125" alt="adminPanel" class="block" />
                <div class="clear"></div>
            </div>

            <div style="padding-right:200px" align="center" id="addFlight">
                <br>
                <div style="padding-right:220px"><strong><font size="5">Add a flight</font></strong></div>
                <br>
                <form action="Company" method="post">
                    <table CLASS="boldtable" border="0" >
                        <tr>
                            <td> Flight Number</td>
                            <td><input name="flightNumber" type="text" size="30" autocomplete="off"/></td>
                        </tr><tr>
                            <td>From</td>
                            <td>
                                <select name="flightFrom" tabindex="10">
                                    <option value="from->">From -></option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Barcelona">Barcelona</option>
                                    <option value="Bucharest" >Bucharest</option>
                                    <option value="Liverpool">Liverpool</option>
                                    <option value="Madrid">Madrid</option>
                                    <option value="Paris">Paris</option>
                                    <option value="Rome">Rome</option>
                                    <option value="Tenerife">Tenerife</option>
                                    <option value="Vienna">Vienna</option>
                                    <option value="Zagreb">Zagreb</option>
                                    <option value="Zurich">Zurich</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>To</td>
                            <td>
                                <select name="flightTo" tabindex="10">
                                    <option value="->to">-> To</option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Barcelona">Barcelona</option>
                                    <option value="Bucharest" >Bucharest</option>
                                    <option value="Liverpool">Liverpool</option>
                                    <option value="Madrid">Madrid</option>
                                    <option value="Paris">Paris</option>
                                    <option value="Rome">Rome</option>
                                    <option value="Tenerife">Tenerife</option>
                                    <option value="Vienna">Vienna</option>
                                    <option value="Zagreb">Zagreb</option>
                                    <option value="Zurich">Zurich</option>
                                </select>
                            </td>
                        </tr><tr>
                            <td> Departure date</td>
                            <td><input name="departureDate" type="text" value="" class="text" id="departureDate" size ="30"/></td>
                        </tr><tr>
                            <td> Departure hour (HH:MM)</td>
                            <td><input type="text" name="departureHour" size="10" /></td>
                        </tr><tr>
                            <td> Arrival hour (HH:MM)</td>
                            <td><input name="arrivalHour" type="text" size="10" /></td>
                        </tr><tr>
                            <td> Return date</td>
                            <td><input name="returnDate" type="text" value="" class="text" id="returnDate" size="30"/></td>
                        </tr><tr>
                            <td> Return departure hour (HH:MM)</td>
                            <td><input type="text" name="returnDepartureHour" size="10" /></td>
                        </tr><tr>
                            <td> Return arrival hour (HH:MM)</td>
                            <td><input name="returnArrivalHour" type="text" size="10" /></td>
                        </tr><tr>
                            <td>Terminal/Gate</td>
                            <td><input name="gate" type="text" size="30" /></td>
                        </tr><tr>
                            <td>Price</td>
                            <td><input name="price" type="text" size="5" />
                                <font size="4">&#8364;</font>
                            </td>
                        </tr><tr>
                            <td colspan="2" align="right">
                                <input type="submit" class="text" value="Request Approval" name="requestApproval" />
                            </td>
                        </tr>
                        ${fillAllFields}
                        ${flightAdded}
                        ${wrongPriceFormat}
                        ${wrongHourFormat}
                    </table>
                </form>
                <div class="clear"></div>
            </div>

            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>
