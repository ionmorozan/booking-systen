<%-- 
    Document   : search
    Created on : Oct 22, 2011, 1:15:27 AM
    Author     : John
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="classes.Ticket"%>
<%@page import="classes.Flight"%>
<%@page import="java.util.List"%>
<%@page import="java.lang.Object"%>
<%@page import="classes.Flight"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.io.*" %> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Flights Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
        <!-- Copyright
            Copyright 2009 Itamar Arjuan
            jsDatePick is distributed under the terms of the GNU General Public License.
        -->
        <link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
        <script type="text/javascript" src="jsDatePick.min.1.3.js"></script>
        <script type="text/javascript">
            window.onload = function(){
                new JsDatePick({
                    useMode:2,
                    target:"leaving",
                    dateFormat:"%d-%M-%Y"
                });
                new JsDatePick({
                    useMode:2,
                    target:"return",
                    dateFormat:"%d-%M-%Y"
                });
            };
        </script>
    </head>
    <body>
        <div id="wrapper">
            <h1><a href="http://"><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            /* instance of database */
                            DAO dao = DAO.getInstance();
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            if(username==null) username="Guest";
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                <form action="Search" method="post">
                    ${allert}
                    <input  type="hidden" name ="onSearchpage"/>
                    <table summary="" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>From</th>
                            <td>
                                <select name="from" tabindex="10">
                                    <option value="from->">From -></option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Barcelona">Barcelona</option>
                                    <option value="Bucharest" >Bucharest</option>
                                    <option value="Liverpool">Liverpool</option>
                                    <option value="Madrid">Madrid</option>
                                    <option value="Paris">Paris</option>
                                    <option value="Rome">Rome</option>
                                    <option value="Tenerife">Tenerife</option>
                                    <option value="Vienna">Vienna</option>
                                    <option value="Zagreb">Zagreb</option>
                                    <option value="Zurich">Zurich</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>To</th>
                            <td>
                                <select name="to" tabindex="10">
                                    <option value="->to">-> To</option>
                                    <option value="Antalya">Antalya</option>
                                    <option value="Barcelona">Barcelona</option>
                                    <option value="Bucharest" >Bucharest</option>
                                    <option value="Liverpool">Liverpool</option>
                                    <option value="Madrid">Madrid</option>
                                    <option value="Paris">Paris</option>
                                    <option value="Rome">Rome</option>
                                    <option value="Tenerife">Tenerife</option>
                                    <option value="Vienna">Vienna</option>
                                    <option value="Zagreb">Zagreb</option>
                                    <option value="Zurich">Zurich</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Leaving date</th>
                            <td><input name="leaving" type="text" value="" class="text" id="leaving"/></td
                        </tr>
                        <tr>
                            <th>Return date</th>
                            <td><input name="return" type="text" value="" class="text" id="return"/></td>
                        </tr>
                        <tr>
                            <th>Passengers</th>
                            <td>
                                <select name="passengers" tabindex="4" class="pass">
                                    <option value="1" selected="selected">1</option>
                                    <option value="2">2</option>
                                    <option value="3" >3</option>
                                    <option value="4">4</option>
                                </select>
                                <input type="submit" value="Search" class="submit" name="search" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                        session = request.getSession(false);
                        String userType = (String) session.getAttribute("userType");
                        if(userType == null) userType="";
                        if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                        if (userType.equals("Company"))
                            {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%
                        } else if (userType.equals("Admin")){
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%
                        } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%
                        }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/search.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>

            <div align="center">
                <%

                            /* what user search */
                            Ticket ticket = dao.getTicket().get(0);
                            dao.delete(ticket.id, ticket.getFrom());

                            /* Format the date to correspond */
                            DateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
                            SimpleDateFormat fromSearch = new SimpleDateFormat("dd-MMM-yyyy");

                            String formatedDateLeave = myFormat.format(fromSearch.parse(ticket.getLeave()));
                            String formatedDateArrival = myFormat.format(fromSearch.parse(ticket.getReturning()));
                            
                            session.setAttribute("ddate", formatedDateLeave);
                            session.setAttribute("adate", formatedDateArrival);
                            session.setAttribute("cityTo", ticket.getTo());
                            
                            /* search trough all my DB to find the deserve ticket*/
                            int i;
                            List<Flight> list = dao.getAllFlights();
                            for (i = 0; i < list.size(); i++) {
                                Flight flight = list.get(i);

                                if (flight.getFlightFrom().equals(ticket.getFrom())
                                        && flight.getFlightTo().equals(ticket.getTo())
                                        && flight.getDepartureDate().equals(ticket.getLeave())
                                        && flight.getReturnDate().equals(ticket.getReturning())) {

                %>
                <form action="Book" method="post">
                    <TABLE CLASS="MYTABLE">
                        <CAPTION CLASS="MYTABLE">
                            <b><font size="5">
                                    <%=ticket.getPassengers() + "*" + flight.getPrice()%>  &#8364;
                                </font></b>
                            <span style="padding-left:300px">
                                <input type="image" src="images/book.gif" width="100" height="20" alt="bookAndPay">
                            </span>
                        </CAPTION>

                        <THEAD >
                            <TR CLASS="MYTABLE">
                                <TH CLASS="MYTABLE">Departure</TH>
                                <TH CLASS="MYTABLE">Departure Time</TH>
                                <TH CLASS="MYTABLE">Arrival Time</TH>
                            </TR>
                        </THEAD>

                        <TBODY>
                            <TR CLASS="MYTABLE">
                                <TD CLASS="MYTABLE"><%=flight.getFlightNumber()%></TD>
                                <TD CLASS="MYTABLE"><%=flight.getFlightFrom()+ " - " + flight.getDepartureHour()%></TD>
                                <TD CLASS="MYTABLE"><%=flight.getFlightTo() + " - " + flight.getArrivalHour()%></TD>
                            </TR>
                        </TBODY>
                    </TABLE>
                    <TABLE CLASS="MYTABLE">

                        <THEAD >
                            <TR CLASS="MYTABLE">
                                <TH CLASS="MYTABLE">Arrival</TH>
                                <TH CLASS="MYTABLE">Departure Time</TH>
                                <TH CLASS="MYTABLE">Arrival Time</TH>
                            </TR>
                        </THEAD>

                        <TBODY>
                            <TR CLASS="MYTABLE">
                                <TD CLASS="MYTABLE"><%=flight.getFlightNumber()%></TD>
                                <TD CLASS="MYTABLE"><%=flight.getFlightTo()+ " - " + flight.getReturnDepartureHour()%></TD>
                                <TD CLASS="MYTABLE"><%=flight.getFlightFrom()+ " - " + flight.getReturnArrivalHour()%></TD>
                            </TR>

                        </TBODY>
                    </TABLE>
                    <br/>
                </form>
                <%
                                }
                            }
                %>
                <div class="clear"></div>
            </div>

            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>