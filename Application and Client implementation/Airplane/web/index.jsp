<%--
    Document   : index
    Created on : Oct 10, 2011, 6:41:59 PM
    Author     : John
--%>

<%@page import="java.util.List"%>
<%@page import="classes.Ticket"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <title>Airplane Tickets Booker</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <!-- Copyright 
        Copyright 2009 Itamar Arjuan
	jsDatePick is distributed under the terms of the GNU General Public License.
    -->
    <link rel="stylesheet" type="text/css" media="all" href="jsDatePick_ltr.min.css" />
    <script type="text/javascript" src="jsDatePick.min.1.3.js"></script>
    <!-- Java script handler for date choose-->
    <script type="text/javascript">
        window.onload = function(){
            new JsDatePick({
                useMode:2,
                target:"leaving",
                dateFormat:"%d-%M-%Y"
            });
            new JsDatePick({
                useMode:2,
                target:"return",
                dateFormat:"%d-%M-%Y"
            });
        };
    </script>

</head>
<body>
    <div id="wrapper">
        <h1><a href="http://"><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
        <div id="booking">
            <%
                        session = request.getSession(false);
                        String username = (String) session.getAttribute("username");
                        if (username == null) {
                            username = "Guest";
                        }
            %>
            <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
            <form action="Search" method="post">
                ${allert}
                <table summary="" cellspacing="0" cellpadding="0" border="0">
                    <tr><!-- destionations -->
                        <th>From</th>
                        <td>
                            <select name="from" tabindex="9">
                                <option value="Antalya" selected="selected">Antalya</option>
                                <option value="Barcelona">Barcelona</option>
                                <option value="Bucharest" >Bucharest</option>
                                <option value="Liverpool">Liverpool</option>
                                <option value="Madrid">Madrid</option>
                                <option value="Paris">Paris</option>
                                <option value="Rome">Rome</option>
                                <option value="Tenerife">Tenerife</option>
                                <option value="Vienna">Vienna</option>
                                <option value="Zagreb">Zagreb</option>
                                <option value="Zurich">Zurich</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>To</th>
                        <td>
                            <select name="to" tabindex="9">
                                <option value="Antalya" selected="selected">Antalya</option>
                                <option value="Barcelona">Barcelona</option>
                                <option value="Bucharest" >Bucharest</option>
                                <option value="Liverpool">Liverpool</option>
                                <option value="Madrid">Madrid</option>
                                <option value="Paris">Paris</option>
                                <option value="Rome">Rome</option>
                                <option value="Tenerife">Tenerife</option>
                                <option value="Vienna">Vienna</option>
                                <option value="Zagreb">Zagreb</option>
                                <option value="Zurich">Zurich</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>Leaving date</th>
                        <td><input name="leaving" type="text" value="" class="text" id="leaving"/></td
                    </tr>
                    <tr>
                        <th>Return date</th>
                        <td><input name="return" type="text" value="" class="text" id="return"/></td>
                    </tr>
                    <tr>
                        <th>Passengers</th>
                        <td>
                            <select name="passengers" tabindex="4" class="pass">
                                <option value="1" selected="selected">1</option>
                                <option value="2">2</option>
                                <option value="3" >3</option>
                                <option value="4">4</option>
                            </select>
                            <input type="submit" value="Search" class="submit" name="search" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <!-- end booking -->
        <div id="nav">
            <ul>
                <li><a href="index.jsp">Home</a></li>
                <%
                            String userType = (String) session.getAttribute("userType");
                            if (userType == null) {
                                userType = "";
                            }
                            if (username.equals("Guest")) {
                %>
                <li><a href="login.jsp">Login</a></li>
                <li><a href="register.jsp">Register</a></li>
                <%      } else {
                %>
                <li><a href="login.jsp">Logout</a></li>
                <li><a href="profile.jsp">Profile</a></li>
                <%                          }
                            if (userType.equals("Company")) {
                %>
                <li><a href="company.jsp">Panel</a></li>
                <%                                    } else if (userType.equals("Admin")) {
                %>
                <li><a href="adminPanel.jsp">Panel</a></li>
                <%                                    } else {
                %>
                <li><a href="admin.jsp">Administrator</a></li>
                <%                                    }
                %>
                <li><a href="contact.jsp">Contact</a></li>
            </ul>
        </div>
        <!-- end nav -->
        <h2  id="packagesheader"><img src="images/blue_bar.jpg" width="352" height="23" alt="our packages" /></h2>
        <div id="packages">
            <h3 class="golden">Online Check-in</h3>
            <p>If you are already an user of this site now you can check-in online after you book a ticket.</p>
            <p>Note! You can do check-in online only with 24 hours prior your flight, based on your
	   reservation code.</p>
            <p class="readmore"><a href="checkInOnline.jsp"><img src="images/checkin.gif" width="68" height="15" alt="readmore" /></a></p>
            <h3 class="silveren" ><i>"Be a part of our team"</i></h3>
            <p>Don't be late to create an account to get the best deals and have a great time.</p>
            <p class="readmore"><a href="register.jsp"><img src="images/register.jpg" width="70" height="20" alt="readmore" /></a></p>
            <div id="special"> <a href="http://"><img src="images/ad_special_offer.gif" width="293" height="79" alt="special offer" /></a>
                <br><br>
                <form action="Tests" method="post">
                    <input type="submit" value="Create DataBase!" class="submit" name="tests" />
                </form>
            </div>
            <!-- end special features -->
        </div>
        <div id="main"> <img src="images/people.jpg" width="447" height="298" alt="airplane" class="block" />
            <h2><img src="images/blue2_bar.jpg" width="447" height="24" alt="bluebar" /></h2>
            <div class="inner">
                <h3 class="blue">Hotels</h3>
                <img src="images/photo_1.jpg" width="109" height="71" alt="hotel" class="left" />
                <p>Now you can get a full packge with our reservation booking system.</p>
                <p>If you're buying a ticket from us you have special prices on the most
		 beautiful hotels in the world.</p>
                <p><a href="reservations.jsp">
                        <img src="images/reservation_button.jpg" width="90" height="20" alt="reservation_button" align="right"/>
                    </a>
                </p>
                <br>
                <h3 class="green">Rent a Car</h3>
                <img src="images/photo_2.gif" width="109" height="71" alt="car" class="left" />
                <p>If you're looking for a beautiful car on your vacation now you can book one.</p>
                <p>Important! You will get a 10% discount at your reservation, booking an airplane ticket
	     from us.</p>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <!-- end main -->
        <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
        <!-- end footer -->
    </div>
</body>
</html>
