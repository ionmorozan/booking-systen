<%-- 
    Document   : profile
    Created on : Oct 23, 2011, 3:13:21 PM
    Author     : John
--%>

<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <form action="Profile" method="post">
            <div id="wrapper">
                <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
                <div id="booking">
                    <%
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            if(username==null) username="Guest";
                    %>
                    <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                </div>
                <!-- end booking -->
                <div id="nav">
                    <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <%
                            String userType = (String) session.getAttribute("userType");
                            if(userType == null) userType="";
                            if (username.equals("Guest")) {
                        %>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <%      } else {
                        %>
                        <li><a href="login.jsp">Logout</a></li>
                        <li><a href="profile.jsp">Profile</a></li>
                        <%                          }
                            if (userType.equals("Company"))
                                {
                        %>
                        <li><a href="company.jsp">Panel</a></li>
                        <%
                            } else if (userType.equals("Admin")){
                        %>
                        <li><a href="adminPanel.jsp">Panel</a></li>
                        <%
                            } else {
                        %>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <%
                            }
                        %>
                        <li><a href="contact.jsp">Contact</a></li>
                    </ul>
                </div>

                <div id="main">
                    <img src="images/contact2.jpg" width="447" height="125" alt="login" class="block" />
                    <div class="clear"></div>
                </div>

               
                <div align="center" id="userTable">
                     ${errorMessage}
                    <div style="padding-top:20px"><strong><font size="5">Profile Settings</font></strong></div><br>

                    <table CLASS="boldtable" border="0" >
                        <tr>
                            <td> First Name*</td>
                            <td><input name="firstName" type="text" size="30"/></td>
                        </tr><tr>
                            <td> Last Name*</td>
                            <td><input name="lastName" type="text" size="30" /></td>
                        </tr><tr>
                            <td> E-mail Address*</td>
                            <td><input type="text" name="email" size="30" autocomplete="off"/></td>
                        </tr><tr>
                            <td> Phone*</td>
                            <td><input name="phone" type="text" size="30" /></td>
                        </tr><tr>
                            <td><strong><font size="3">Address(Optional)</font></strong></td>
                        </tr><tr>
                            <td>Street/No</td>
                            <td><input name="street" type="text" size="30" /></td>
                        </tr><tr>
                            <td>City</td>
                            <td><input name="city" type="text" size="30" /></td>
                        </tr><tr>
                            <td>Department</td>
                            <td><input name="department" type="text" size="30" /></td>
                        </tr><tr>
                            <td><INPUT type="reset" name="reset" /></td>
                            <td align="right"><input type="submit" name="submit" value="Update Profile"></td>
                        </tr>
                        <tr>
                            <td><font size="2"><br/>* Required fields!</font></td>
                        </tr>
                    </table>
                    <div class="clear"></div>

                </div>

                <!-- end main -->
                <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
                <!-- end footer -->
            </div>
        </form>
    </body>
</html>




