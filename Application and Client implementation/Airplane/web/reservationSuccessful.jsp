<%-- 
    Document   : reservationSuccessful
    Created on : 28.11.2011, 23:06:13
    Author     : Ion Morozan & Andreea Sandu
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Availability"%>
<%@page import="classes.DAO"%>
<%@ page import="java.util.Random;" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            session = request.getSession(false);
                            String username = (String) session.getAttribute("username");
                            if (username == null) {
                                username = "Guest";
                            }
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->

            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                                session = request.getSession(false);
                                String userType = (String) session.getAttribute("userType");
                                if (userType == null) {
                                    userType = "";
                                }
                                if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                                if (userType.equals("Company")) {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%                                    } else if (userType.equals("Admin")) {
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%                                    } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%                                    }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/payment.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>
            
            <div align="center" style="padding-top: 25px">
                <h2><font size="3" color="green">Your reservation has been successfully completed!</font></h2><br>
                <p><a href="index.jsp">Home</a></p>
            </div>
            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>