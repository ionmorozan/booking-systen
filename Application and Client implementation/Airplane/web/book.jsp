<%-- 
    Document   : book
    Created on : Oct 22, 2011, 3:17:07 PM
    Author     : John
--%>

<%@page import="classes.Profile"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            if(username==null) username="Guest";
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                <br>
                <div class="jtype" style="padding-left:22px" style="padding-top:100px">
                    <img align ="center" src="images/card.jpg" width="200" height="120" alt="card"/>
                </div>
            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                        DAO dao = DAO.getInstance();
                        Profile profileCurrent;
                        if (!username.equals("Guest"))
                            profileCurrent = (Profile)dao.get(2, username);
                        else
                            profileCurrent = new Profile();

                        if (profileCurrent.getFirstName() == null) profileCurrent.setFirstName("");
                        if (profileCurrent.getLastName() == null) profileCurrent.setLastName("");
                        if (profileCurrent.getEmail() == null) profileCurrent.setEmail("");
                        if (profileCurrent.getStreet() == null) profileCurrent.setStreet("");
                        if (profileCurrent.getCity() == null) profileCurrent.setCity("");
                        if (profileCurrent.getDepartment() == null) profileCurrent.setDepartment("");

                        String userType = (String) session.getAttribute("userType");
                        if(userType == null) userType="";
                        if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                        if (userType.equals("Company"))
                            {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%
                        } else if (userType.equals("Admin")){
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%
                        } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%
                        }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/payment.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>

            <div align="center" style="padding-top: 25px">
                <form action="Pay" method="post">
                    ${errorMessage}
                    ${wrongCardFormat}
                    <table CLASS="boldtable" width=518 border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                        <tr bgcolor="#E5E5E5">
                            <td height="22 " colspan="3" align="left" valign="middle">
                                <strong>&nbsp;Billing Information (required)</strong>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" width="180" align="right" valign="middle">First Name*:</td>
                            <td colspan="2" align="left"><input name="firstName" value="<%=profileCurrent.getFirstName()%>" type="text" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Last Name*:</td>
                            <td colspan="2" align="left"><input name="lastName" value="<%=profileCurrent.getLastName()%>" type="text" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Email*:</td>
                            <td colspan="2" align="left"><input name="email" value="<%=profileCurrent.getEmail()%>" type="text" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Street Address:</td>
                            <td colspan="2" align="left"><input name="street" value="<%=profileCurrent.getStreet()%>" type="text" value="" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">City:</td>
                            <td colspan="2" align="left"><input name="city" value="<%=profileCurrent.getCity()%>" type="text" value="" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">State/Province:</td>
                            <td colspan="2" align="left"><input name="state" value="<%=profileCurrent.getDepartment()%>" type="text" value="" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Zip/Postal Code:</td>
                            <td colspan="2" align="left"><input name="zip" type="text" value="" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Country:</td>
                            <td colspan="2" align="left"><input name="country" type="text" value="" size="50"></td>
                        </tr>
                        <tr>
                            <td height="22" colspan="3" align="left" valign="middle">&nbsp;</td>
                        </tr>
                        <tr bgcolor="#E5E5E5">
                            <td height="22" colspan="3" align="left" valign="middle"><strong>&nbsp;Credit Card (required)</strong></td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Type:</td>
                            <td colspan="2" align="left">
                                <SELECT NAME="type">
                                    <OPTION VALUE="" SELECTED>--Type--
                                    <OPTION VALUE="01">Visa
                                    <OPTION VALUE="02">MasterCard
                                    <OPTION VALUE="03">Discover
                                    <OPTION VALUE="04">American Express
                                </SELECT>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" align="right" valign="middle">Credit Card Number*:</td>
                            <td colspan="2" align="left"><input name="cardNo" type="text" value="" size="16" maxlength="16"></td>
                        </tr>

                        <tr>
                            <td height="22" align="right" valign="middle">Expiry Date*:</td>
                            <td colspan="2" align="left">
                                <SELECT NAME="cardExpiresMonth" >
                                    <OPTION VALUE="" SELECTED>--Month--
                                    <OPTION VALUE="01">January (01)
                                    <OPTION VALUE="02">February (02)
                                    <OPTION VALUE="03">March (03)
                                    <OPTION VALUE="04">April (04)
                                    <OPTION VALUE="05">May (05)
                                    <OPTION VALUE="06">June (06)
                                    <OPTION VALUE="07">July (07)
                                    <OPTION VALUE="08">August (08)
                                    <OPTION VALUE="09">September (09)
                                    <OPTION VALUE="10">October (10)
                                    <OPTION VALUE="11">November (11)
                                    <OPTION VALUE="12">December (12)
                                </SELECT> /
                                <SELECT NAME="cardExpiresYear">
                                    <OPTION VALUE="" SELECTED>--Year--
                                    <OPTION VALUE="1">2012
                                    <OPTION VALUE="2">2013
                                    <OPTION VALUE="3">2014
                                    <OPTION VALUE="4">2015
                                    <OPTION VALUE="5">2016
                                    <OPTION VALUE="6">2017
                                    <OPTION VALUE="7">2018
                                    <OPTION VALUE="8">2019
                                </SELECT>
                            </td>
                        </tr>
                        <tr>
                            <td height="22" colspan="3" align="left" valign="middle">* Required fields! &nbsp;</td>
                        </tr>
                    </table>
                    <div style="padding-left:420px">
                        <input type="image" src="images/pay.jpg" width="100" height="30" alt="payment">
                    </div>
                </form>
                <div class="clear"></div>
            </div>

            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>