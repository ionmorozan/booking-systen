<%-- 
    Document   : bookHotel
    Created on : Nov 27, 2011, 2:54:44 PM
    Author     : Ion Morozan & Andreea Sandu
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="classes.Availability"%>
<%@page import="classes.DAO"%>
<%@ page import="java.util.Random;" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div id="wrapper">
            <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
            <div id="booking">
                <%
                            session = request.getSession(false);
                            String username = (String) session.getAttribute("username");
                            if (username == null) {
                                username = "Guest";
                            }
                %>
                <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->

            </div>
            <!-- end booking -->
            <div id="nav">
                <ul>
                    <li><a href="index.jsp">Home</a></li>
                    <%
                                session = request.getSession(false);
                                String userType = (String) session.getAttribute("userType");
                                if (userType == null) {
                                    userType = "";
                                }
                                if (username.equals("Guest")) {
                    %>
                    <li><a href="login.jsp">Login</a></li>
                    <li><a href="register.jsp">Register</a></li>
                    <%      } else {
                    %>
                    <li><a href="login.jsp">Logout</a></li>
                    <li><a href="profile.jsp">Profile</a></li>
                    <%                          }
                                if (userType.equals("Company")) {
                    %>
                    <li><a href="company.jsp">Panel</a></li>
                    <%                                    } else if (userType.equals("Admin")) {
                    %>
                    <li><a href="adminPanel.jsp">Panel</a></li>
                    <%                                    } else {
                    %>
                    <li><a href="admin.jsp">Administrator</a></li>
                    <%                                    }
                    %>
                    <li><a href="contact.jsp">Contact</a></li>
                </ul>
            </div>

            <div id="main">
                <img src="images/payment.jpg" width="447" height="125" alt="login" class="block" />
                <div class="clear"></div>
            </div>
            <%
                        /* instance of database */
                        DAO dao = DAO.getInstance();
                        List<Availability> AviableRooms = dao.ofy().query(Availability.class).list();
                        Availability AvResp = AviableRooms.get(0);
                        if (AviableRooms.get(0).getKind() != null) {
            %>
            <div align="center" style="padding-top: 25px">
                ${errorMessage}
                <h2><font size="3" color="green">Book a hotel room for your stay</font></h2><br>
                <form action="BookHotel" method="post">
                    <table summary="" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <th>Check-in</th>
                            <td><input type=date name=adate value="<%=AvResp.getCheckin()%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>Check-out</th>
                            <td><input type=date name=ddate value="<%=AvResp.getCheckout()%>" readonly="readonly"></td>
                        </tr>
                        <tr>
                            <th>Room Size</th>
                            <td><input type=text name="room_size" value="<%=AvResp.getRoom_size()%>" readonly="readonly" size="1"></td>
                        </tr>
                        <tr>
                            <th>Rooms ID</th>
                            <td>
                                <select name=rooms_id>
                                    <%
                                                                for (int i = 0; i < AvResp.getRooms_id().size(); i++) {
                                    %>
                                    <option><%=AvResp.getRooms_id().get(i)%></option>
                                    <%  }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Kind</th>
                            <td><input type="text" name="kind" value="<%=AvResp.getKind().get(0)%>" readonly="readonly"/></td>
                        </tr>

                        <tr>
                            <th>Description</th>
                            <td>
                                <select name=description>
                                    <%
                                                                for (int i = 0; i < AvResp.getDescription().size(); i++) {
                                    %>
                                    <option><%=AvResp.getDescription().get(i)%></option>
                                    <%  }%>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td><input name="name" type="text" value="" class="text" autocomplete="off"/></td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>
                                <input name="email" type="email" value="" class="text" autocomplete="off"/>
                                <input type="submit" value="Book!" class="submit" name="book" />
                            </td>
                        </tr>
                    </table>

                </form>
            </div>
            <%
                                    } else {
            %>
            <div align="center" style="padding-top: 25px">
                <h2><font size="3" color="red">There are no available rooms between those dates!</font></h2>
                <p><a href="pay.jsp">Go Back!</a></p>
            </div>
            <%                            }
                        /*erase the req that I made from DB cause i dont need anymore */
                        dao.ofy().delete(AviableRooms);

            %>

            <br>
            <!-- end main -->
            <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
            <!-- end footer -->
        </div>
    </body>
</html>