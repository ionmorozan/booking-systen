<%-- 
    Document   : checkInOnline
    Created on : 23.10.2011, 15:13:54
    Author     : Andreea
--%>

<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <form action="CheckInOnline" method="post">
            <div id="wrapper">
                <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
                <div id="booking">
                    <%
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            if(username==null) username="Guest";
                    %>
                    <h2><%="Logged as: " + username%></h2><!-- You are logged as: -->
                </div>
                <!-- end booking -->
               <div id="nav">
                    <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <%
                            session = request.getSession(false);
                            String userType = (String) session.getAttribute("userType");
                            if(userType == null) userType="";
                            if (username.equals("Guest")) {
                        %>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <%      } else {
                        %>
                        <li><a href="login.jsp">Logout</a></li>
                        <li><a href="profile.jsp">Profile</a></li>
                        <%                          }
                            if (userType.equals("Company"))
                                {
                        %>
                        <li><a href="company.jsp">Panel</a></li>
                        <%
                            } else if (userType.equals("Admin")){
                        %>
                        <li><a href="adminPanel.jsp">Panel</a></li>
                        <%
                            } else {
                        %>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <%
                            }
                        %>
                        <li><a href="contact.jsp">Contact</a></li>
                    </ul>
                </div>

                <div id="main">
                    <img src="images/pic4.jpg" width="447" height="125" alt="login" class="block" />
                    <div class="clear"></div>
                </div>

                <div align="center">
                    <table summary="" cellspacing="0" cellpadding="5" border="0">
                        <tr>
                            <th>Username</th>
                            <td><input name="username" type="text" value="" class="text" autocomplete="off"/></td>
                        </tr><tr>
                            <th>Password</th>
                            <td><input name="password" type="password" class="text" autocomplete="off"/></td>
                        </tr><tr>
                            <th>Confirmation Number</th>
                            <td><input name="confNumber" type="text" value="" class="text" autocomplete="off"/></td>
                        </tr><tr>
                            <td colspan="2" align="center"><input type="submit" value="Check In Online" name="checkInOnline" /></td>
                        </tr>
                        ${errorMessage}
                    </table>
                    <p><font color="red" size="3"><strong>Note!</strong></font>
                        <font size="2">Check in Online is available only for <a href="register.jsp">registered</a> user!</font>
                    </p>
                    <div class="clear"></div>
                </div>

                <!-- end main -->
                <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
                <!-- end footer -->
            </div>
        </form>
    </body>
</html>
