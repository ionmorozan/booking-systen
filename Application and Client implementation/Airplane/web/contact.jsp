<%-- 
    Document   : contact
    Created on : Oct 23, 2011, 8:13:16 PM
    Author     : John
--%>

<%@page import="java.util.List"%>
<%@page import="classes.DAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <title>Airplane Tickets Booker</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="style.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <form action="Login" method="post">
            <div id="wrapper">
                <h1><a href=""><img src="images/logo.gif" width="554" height="47" alt="Travel Agency" /></a></h1>
                <div id="booking">
                    <!--radio button to select the account type -->
                    <div class="jtype">
                        <img style="padding-left: 20px; padding-top: 10px" src="images/cvut.jpg" width="200" height="150" alt="cvut"/>
                    </div>
                </div>
                <!-- end booking -->
                <div id="nav">
                    <ul>
                        <li><a href="index.jsp">Home</a></li>
                        <%
                            session = request.getSession(false);
                            String username=(String) session.getAttribute("username");
                            String userType = (String) session.getAttribute("userType");
                            if(username==null) username="Guest";
                            if(userType == null) userType="";
                            if (username.equals("Guest")) {
                        %>
                        <li><a href="login.jsp">Login</a></li>
                        <li><a href="register.jsp">Register</a></li>
                        <%      } else {
                        %>
                        <li><a href="login.jsp">Logout</a></li>
                        <li><a href="profile.jsp">Profile</a></li>
                        <%                          }
                            if (userType.equals("Company"))
                                {
                        %>
                        <li><a href="company.jsp">Panel</a></li>
                        <%
                            } else if (userType.equals("Admin")){
                        %>
                        <li><a href="adminPanel.jsp">Panel</a></li>
                        <%
                            } else {
                        %>
                        <li><a href="admin.jsp">Administrator</a></li>
                        <%
                            }
                        %>
                        <li><a href="contact.jsp">Contact</a></li>
                    </ul>
                </div>

                <div id="main">
                    <img src="images/contact.jpg" width="447" height="125" alt="login" class="block" />
                    <div class="clear"></div>
                </div>

                <div align="center">
                    <div style="padding-top:50px"><strong><font size="5">Team/Developers</font></strong>

                        <strong><font size="2">
                                <li>Morozan Ion - morozion@fel.cvut.cz</li>
                                <li>Sandu Andreea - sanduand@fel.cvut.cz</li>
                            </font></strong>
                    </div>
                    <div style="padding-top:50px"><strong><font size="5">University</font></strong>
                        <p>
                            <strong><font size="3">
                                    Czech Technical University , Prague @2011
                                </font></strong>
                        </p>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- end main -->
                <div id="footer"> &copy; Copyright Mdw11Ws201tm4  2011 | designed by Morozan Ion &amp; Sandu Andreea </div>
                <!-- end footer -->
            </div>
        </form>
    </body>
</html>