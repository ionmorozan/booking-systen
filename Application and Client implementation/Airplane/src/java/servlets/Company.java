package servlets;

import classes.DAO;
import classes.Flight;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The company can add flights to the database so that users can find and buy
 * @author Sandu Andreea & Morozan Ion
 */
public class Company extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/company.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String flightNumber = request.getParameter("flightNumber");
        String flightFrom = request.getParameter("flightFrom");
        String flightTo = request.getParameter("flightTo");
        String departureDate = request.getParameter("departureDate");
        String departureHour = request.getParameter("departureHour");
        String arrivalHour = request.getParameter("arrivalHour");
        String returnDate = request.getParameter("returnDate");
        String returnDepartureHour = request.getParameter("returnDepartureHour");
        String returnArrivalHour = request.getParameter("returnArrivalHour");
        String gate = request.getParameter("gate");
        String price = request.getParameter("price");

        /* a new ticket */
        Flight ticket = new Flight(flightNumber, flightFrom, flightTo, departureDate,
                departureHour, arrivalHour, returnDate, returnDepartureHour,
                returnArrivalHour, gate, price);

        DAO dao = DAO.getInstance();

        if (flightNumber.isEmpty() || flightFrom.isEmpty() || flightTo.isEmpty() ||
                departureDate.isEmpty() || departureHour.isEmpty() ||
                arrivalHour.isEmpty() || returnDate.isEmpty() ||
                returnDepartureHour.isEmpty() || returnArrivalHour.isEmpty() ||
                price.isEmpty() || gate.isEmpty()) {
            /* all required fields must be filled in */
            request.setAttribute("fillAllFields", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the fields! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
        }
        else if (!price.matches("[0-9]+")) {
            /* the price must contain only digits */
            request.setAttribute("wrongPriceFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Price Format! Must contains only numbers </font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
        }
        else if (!departureHour.matches("[0-9]+:[0-9]+") ||
                !arrivalHour.matches("[0-9]+:[0-9]+") ||
                !returnDepartureHour.matches("[0-9]+:[0-9]+") ||
                !returnArrivalHour.matches("[0-9]+:[0-9]+")) {
            /* the arrival and departure hours must follow the format HH:MM */
            request.setAttribute("wrongHourFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Hour Format! The right format is HH:MM </font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
        }
        else {
            dao.put(ticket);

            request.setAttribute("flightAdded", "<font size=\"3\" "
                    + "face=\"arial\" color=\"green\">"
                    + "Your flight was added! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
            response.sendRedirect("company.jsp");
        }
    }

}
