/**
 * The user can update his reservation based on his Reservation ID
 *
 * @author Sandu Andreea & Morozan Ion
 */
package servlets;

import classes.HotelReservation;
import javax.servlet.http.HttpSession;
import classes.DAO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class UpdateReservation extends HttpServlet {

    private String Rid;
    private String Rstart;
    private String Rend;
    private String RroomId;
    private String RnrOfPers;
    private String Rdesc;
    private String Rname;
    private String Remail;

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/reservations.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String reservationId = request.getParameter("reservationid");
        String aDate = request.getParameter("adate");
        String rDate = request.getParameter("rdate");
        String updateReservation = request.getParameter("updateReservation");

        /* instance of database */
        DAO dao = DAO.getInstance();

        /*current date*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        /*useful to extract the current user*/
        HttpSession session = request.getSession(false);
        String username = (String) session.getAttribute("username");

        /* if user press the Update Reservation Button */
        if (updateReservation != null) {
            /* all fields must be filled && and reservation id must match with the one from DB*/
            if (reservationId.isEmpty() || aDate.isEmpty() || rDate.isEmpty()
                    || dao.ofy().find(HotelReservation.class, reservationId) == null) {
                request.setAttribute("errorMessage", "<font size=\"2\" "
                        + "face=\"arial\" color=\"red\">"
                        + "Invalid Reservation id! <br/>"
                        + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("reservations.jsp");
                rd.forward(request, response);
                /* The date must be in a logical order*/
            } else if ((aDate.compareTo(rDate) > 0)
                    || (aDate.compareTo(dateFormat.format(date).toString()) <= 0)) {
                request.setAttribute("errorMessage", "<font size=\"2\" "
                        + "face=\"arial\" color=\"red\">"
                        + "Wrong Date! <br/>"
                        + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("reservations.jsp");
                rd.forward(request, response);
            } else {
                /*
                 * PUT method
                 */
                /* extract from the DB the reservation that the user want to update*/
                HotelReservation HR = dao.ofy().find(HotelReservation.class, reservationId);

                /*create the query link*/
                String link = "http://mdwhotel.appspot.com/api/v1/reservation/?res_id=";
                link += reservationId + "&start=" + aDate + "&end=" + rDate + "&email=" + HR.getEmail();

                Client client = Client.create();
                WebResource webResource = client.resource(link);
                webResource.accept("application/xml");
                /*invoke the call*/
                String resp = webResource.put(String.class);

                /* if the application sends me back an XML who contains this message
                 * means that they don't have available rooms between thhose dates
                 * and I'm not deleting the reservation that he made before
                 */
                if (!resp.contains("Sorry - the room is not free in these days")) {

                    /*delete the last reservation because they will send to me anotherone*/
                    dao.ofy().delete(HR);
                    /* parse the XML file that I receive from them*/
                    StringToXML(resp);
                    /* set the new information of the reservation*/
                    HR.setStart(Rstart);
                    HR.setEnd(Rend);
                    HR.setDesc(Rdesc);
                    HR.setEmail(Remail);
                    HR.setId(Rid);
                    HR.setName(Rname);
                    HR.setRoomId(RroomId);
                    HR.setNrOfPers(RnrOfPers);
                    dao.put(HR);
                } else {
                    /* if they don't have available rooms between those date 
                     * a message retrun
                     */
                    request.setAttribute("errorMessage", "<font size=\"2\" "
                            + "face=\"arial\" color=\"red\">"
                            + "No aviable rooms!<br/>"
                            + "</font>");
                }
                /* successful */
                request.setAttribute("errorMessage", "<font size=\"2\" "
                        + "face=\"arial\" color=\"green\">"
                        + "Reservation successfully updated!<br/>"
                        + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("reservations.jsp");
                rd.forward(request, response);
                response.sendRedirect("reservations.jsp");
            }
            /* if delete reservation button is pressed*/
            /* verify if the id is correct */
        } else if (reservationId.isEmpty()
                || dao.ofy().find(HotelReservation.class, reservationId) == null) {
            request.setAttribute("errorMessage", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid Reservation id! <br/>"
                    + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("reservations.jsp");
            rd.forward(request, response);
        } else {
            /*
             * DELETE method
             */
            /* extract the desired reservation from DB*/
            HotelReservation HR = dao.ofy().find(HotelReservation.class, reservationId);

            /* create the query link */
            String link = "http://mdwhotel.appspot.com/api/v1/reservation/?res_id=";
            link += reservationId + "&email=" + HR.getEmail();


            Client client = Client.create();
            WebResource webResource = client.resource(link);
            webResource.accept("application/xml");
            /*invoke method */
            String resp = webResource.delete(String.class);

            /*delete the reservation from the DB*/
            dao.ofy().delete(HotelReservation.class, reservationId);
            /* successful */
            request.setAttribute("errorMessage", "<font size=\"2\" "
                    + "face=\"arial\" color=\"green\">"
                    + "Reservation successfully deleted!<br/>"
                    + "</font>");

            RequestDispatcher rd = request.getRequestDispatcher("reservations.jsp");
            rd.forward(request, response);
            response.sendRedirect("reservations.jsp");

        }
    }
    /**
     * Parse the xml file and extract the desired fields
     * @param xmlString = xml file receive from the remote application
     */
    protected void StringToXML(String xmlString) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {

            builder = factory.newDocumentBuilder();
            // Use String reader
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("reservation");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    Rid = getTagValue("id", eElement);
                    Rstart = getTagValue("start", eElement);
                    Rend = getTagValue("end", eElement);
                    RroomId = getTagValue("roomId", eElement);
                    RnrOfPers = getTagValue("nrOfPersons", eElement);
                    Rdesc = getTagValue("description", eElement);
                    Rname = getTagValue("username", eElement);
                    Remail = getTagValue("email", eElement);
                }
            }
        } catch (Exception e) {
            System.out.println("Eroare la parsare XML ");
        }
    }
    /*
     * Extract the Tag and what contains
     */
    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }
}
