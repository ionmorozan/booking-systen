package servlets;

import classes.Profile;
import classes.User;
import classes.DAO;
import classes.CompanyProfile;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Used when a new user wants to register (user or company)
 * @author Morozan Ion & Sandu Andreea
 */

public class Register extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

   /**
     * This method is used if a user wants to register
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void userSelected(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String phone = request.getParameter("phone");
        String street = request.getParameter("street");
        String city = request.getParameter("city");
        String department = request.getParameter("department");
        String passwordAgain = request.getParameter("passwordAgain");

        /* create new user */
        Profile profile = new Profile(username, password, firstName, lastName,
                email, phone, street, city, department);
        /* instance of database */
        DAO dao = DAO.getInstance();

        /* all fields must be filled */
        if (username.isEmpty() || password.isEmpty() || firstName.isEmpty()
                || lastName.isEmpty() || email.isEmpty() || phone.isEmpty()) {
            request.setAttribute("fillAllFields", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the fields! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* wrong phone format*/
        } else if (!phone.matches("[0-9]+")) {
            request.setAttribute("wrongPhoneFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Phone Format! Must contains only numbers </font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* username already exists */
        } else if (dao.exists(profile.getId(), username)) {
            request.setAttribute("userAlreadyExists", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Username already exists! Choose another one!</font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /*check if password matches */
        } else if (!passwordAgain.equals(password)) {
            request.setAttribute("passDontMatch", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Password dosen't match!</font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* add the user to the database*/
        } else {
            /*create an user for the login database */
            User user = new User(username, password, "User");

            /*create admin*/
            //User user = new User(username, password, "Admin");

            dao.put(user);

            dao.put(profile);
            request.setAttribute("username", "<font size=\"3\" "
                    + "face=\"arial\"> You are logged as " + username + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
            rd.forward(request, response);
            response.sendRedirect("login.jsp");
        }

    }

   /**
     * This method is used if a company wants to register
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void companySelected(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String firstName = request.getParameter("nameCom");
        String regNumber = request.getParameter("regNumber");
        String regAuth = request.getParameter("regAuth");
        String email = request.getParameter("emailCom");
        String username = request.getParameter("usernameCom");
        String password = request.getParameter("passwordCom");
        String phone = request.getParameter("phoneCom");
        String street = request.getParameter("streetCom");
        String city = request.getParameter("cityCom");
        String department = request.getParameter("departmentCom");
        String passwordAgain = request.getParameter("passwordAgainCom");

        
        /* create new user */
        CompanyProfile profile = new CompanyProfile(username, password, firstName,
                regNumber, regAuth, email, phone, street, city, department);
        /* instance of database */
        DAO dao = DAO.getInstance();

        /* all fields must be filled */
        if (username.isEmpty() || password.isEmpty() || firstName.isEmpty()
                || regAuth.isEmpty() || regNumber.isEmpty() || email.isEmpty()
                || phone.isEmpty()) {
            request.setAttribute("fillAllFields", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the fields! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* wrong phone format*/
        } else if (!phone.matches("[0-9]+")) {
            request.setAttribute("wrongPhoneFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Phone Format! Must contains only numbers </font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /*wrong registration number format*/
        } else if (!regNumber.matches("[0-9]+")){
            request.setAttribute("wrongRegNumberFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Registration Number Format! Must contains only numbers </font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* username already exists */
        } else if (dao.exists(profile.id, username)) {
            request.setAttribute("userAlreadyExists", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Username already exists! Choose another one!</font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /*check if password matches */
        } else if (!passwordAgain.equals(password)) {
            request.setAttribute("passDontMatch", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Password dosen't match!</font>");
            RequestDispatcher rd = request.getRequestDispatcher("register.jsp");
            rd.forward(request, response);
            /* add the user to the database*/
        } else {
            /*create an user for the login database */
            User user = new User(username, password, "Company");
            dao.put(user);

            dao.put(profile);
            request.setAttribute("username", "<font size=\"3\" "
                    + "face=\"arial\"> You are logged as " + username + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
            response.sendRedirect("company.jsp");
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userType = request.getParameter("group");

        if (userType.equals("User"))
            /* if an user is going to register */
            userSelected(request,response);
        else
            /* if a company is creating a new account */
            companySelected(request, response);
    }
}
