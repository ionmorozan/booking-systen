package servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * When the client wants to pay a ticket, the required fields must be filled
 * in correctly
 * @author Morozan Ion & Sandu Andreea
 */
public class Pay extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String state = request.getParameter("state");
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        String zip = request.getParameter("zip");
        String type = request.getParameter("type");
        String cardNo = request.getParameter("cardNo");
        String CardExpiresMonth = request.getParameter("cardExpiresMonth");
        String CardExpiresYear = request.getParameter("cardExpiresYear");


        /* all fields must be filled */
        if (CardExpiresMonth.isEmpty() || cardNo.isEmpty() || firstName.isEmpty()
                || lastName.isEmpty() || email.isEmpty() || type.isEmpty()
                || CardExpiresYear.isEmpty()) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the required fields! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
            rd.forward(request, response);
            /* wrong card number format*/
        } else if (!cardNo.matches("[0-9]+")) {
            request.setAttribute("wrongCardFormat", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Credit Card Number must contains only numbers! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
            rd.forward(request, response);
            /* credit card not to short or to long*/
        } else if (cardNo.length() < 16 || cardNo.length() > 16) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Credit card number must have 16 digits</font>");
            RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
            rd.forward(request, response);
        } else {
            response.sendRedirect("pay.jsp");
        }
    }
}
