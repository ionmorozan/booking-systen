package servlets;

import classes.User;
import classes.DAO;

import java.io.IOException;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * The main login page - the username, password and group are verified
 * @author Morozan Ion & Sandu Andreea
 */
public class Login extends HttpServlet {
    
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userType = request.getParameter("group");
        
        /* create new user */
        User user = new User(username, password, userType);

        /* instance of database */
        DAO dao = DAO.getInstance();
        
        /* all fields must be filled && pass must match */
        if (username.isEmpty() || password.isEmpty() || userType == null
                || !dao.exists(user.id, username)
                /*extract the pass from the DB and compare with the one that
                the user enters now*/
                || !password.equals(((User) dao.get(user.id, username)).password)
                || !userType.equals(((User) dao.get(user.id, username)).group)) {

            HttpSession session = request.getSession(false);
            session.removeAttribute("username");
            session.removeAttribute("userType");

            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid username or password. Please try again! <br/>"
                    + "Note! If you don't have an account, please"
                    + " <a href=\"register.jsp\"> register!</a></font>");
            RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
            rd.forward(request, response);
        } else {

            HttpSession session = request.getSession(true);
            session.setAttribute("username", username);
            session.setAttribute("userType", userType);
            
            if (userType.equals("User")) {
                request.setAttribute("username", "<font size=\"2\" "
                        + username + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.forward(request, response);
                response.sendRedirect("index.jsp");
            } else {
                request.setAttribute("username", "<font size=\"2\" "
                        + username + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
                rd.forward(request, response);
                response.sendRedirect("company.jsp");
            }
        }

    }
}
