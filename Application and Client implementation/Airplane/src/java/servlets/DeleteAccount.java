package servlets;

import classes.*;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used when a company wants to deactivate their account
 * @author Morozan Ion & Sandu Andreea
 */
public class DeleteAccount extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/company.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String usernameDel = request.getParameter("usernameDel");
        String passwordDel = request.getParameter("passwordDel");
        String userTypeDel = "Company";

        /* create new user */
        User user = new User(usernameDel, passwordDel, userTypeDel);

        /* instance of database */
        DAO dao = DAO.getInstance();

        /* all fields must be filled && pass must match */
        if (usernameDel.isEmpty() || passwordDel.isEmpty() || !dao.exists(user.id, usernameDel)
                /*extract the pass from the DB and compare with the one that
                 the user enters now*/
                || !passwordDel.equals(((User) dao.get(user.id, usernameDel)).password)) {
            request.setAttribute("errorMessage", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid username or password!<br/>"
                    + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("company.jsp");
            rd.forward(request, response);
        } else {
            /* delete the account of the company*/
            dao.delete(4, usernameDel);
            dao.delete(1, usernameDel);

            RequestDispatcher rd = request.getRequestDispatcher("successfullyDeleted.jsp");
            rd.forward(request, response);
            response.sendRedirect("successfullyDeleted.jsp");
            }
        }

}
