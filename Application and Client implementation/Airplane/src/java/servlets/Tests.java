package servlets;

import classes.CompanyProfile;
import classes.User;
import classes.DAO;
import classes.Flight;
import classes.Profile;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Tests extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String state = request.getParameter("state");
        String city = request.getParameter("city");
        String street = request.getParameter("street");
        String zip = request.getParameter("zip");
        String type = request.getParameter("type");
        String cardNo = request.getParameter("cardNo");
        String CardExpiresMonth = request.getParameter("cardExpiresMonth");
        String CardExpiresYear = request.getParameter("cardExpiresYear");

        /* instance of database */
        DAO dao = DAO.getInstance();


        /* create new users - There are different types of users: Normal
         * Users and Company Users
         * Note! At this DB model the unique ID si the username
         */

        //Normal Users
        User user = new User("ion.morozan", "test", "User");
        dao.put(user);

        user = new User("milan", "test", "User");
        dao.put(user);

        user = new User("andreea.sandu", "test", "User");
        dao.put(user);

        //Company Users
        User companyUser = new User("KLM", "test", "Company");
        dao.put(companyUser);

        companyUser = new User("Luthansa", "test", "Company");
        dao.put(companyUser);

        companyUser = new User("AirFrance", "test", "Company");
        dao.put(companyUser);


        //Admin User
        User admin = new User("admin", "admin", "Admin");
        dao.put(admin);


        //User Profile
        Profile profile = new Profile("ion.morozan", "test", "ion", "morozan",
                "ion.morozan@gmail.com", "0745342231", "Kolej Strahov", "Prague", "6");
        dao.put(profile);
        profile = new Profile("andreea.sandu", "test", "andreea", "sandu",
                "andreea.sandu@cti.cz", "0745343453", "Kolej Strahov", "Prague", "6");
        dao.put(profile);
        profile = new Profile("milan", "test", "milan", "milan",
                "milan@milan.com", "0745111111", "Devijka", "Prague", "6");
        dao.put(profile);

         //Company Profile
        CompanyProfile companyProfile = new CompanyProfile("KLM", "test", "KLM",
                "123456789", "No12403", "klm@klm.com", "0765342167", "Vanickova", "Prague", "6");
        dao.put(companyProfile);
        companyProfile = new CompanyProfile("Luthansa", "test", "Luthansa",
                "234232423", "No23403", "lht@lht.com", "0765342167", "Mustek", "Prague", "1");
        dao.put(companyProfile);
        companyProfile = new CompanyProfile("AirFrance", "test", "AirFrance",
                "23545645322", "No1223443", "afr@kafr.com", "072756443", "Center", "Prague", "1");
        dao.put(companyProfile);


        /*
         * Add new Flight - this feature can be use only by the
         * airline companies
         * Note! The unique identifier is the flight number
         */

        // Flight from Barcelona - Prais
        Flight flight = new Flight("KLM1200", "Barcelona", "Paris", "25-NOV-2012",
                "12:54", "3:15", "30-NOV-2012", "13:45", "4:25", "A7", "250");
        dao.put(flight);
        flight = new Flight("LHT213", "Barcelona", "Paris", "25-NOV-2012",
                "18:00", "21:20", "30-NOV-2012", "1400", "18:00", "C34", "280");
        dao.put(flight);
        flight = new Flight("AFR4502", "Barcelona", "Paris", "25-NOV-2012",
                "21:25", "23:45", "30-NOV-2012", "8:45", "12:05", "B9", "300");
        dao.put(flight);


        //Flight from Liverpool to Madrid
        flight = new Flight("KLM1400", "Liverpool", "Madrid", "25-NOV-2012",
                "18:20", "21:20", "30-NOV-2012", "14:45", "18:05", "B5", "320");
        dao.put(flight);
        flight = new Flight("LHT21211", "Liverpool", "Madrid", "25-NOV-2012",
                "18:00", "21:20", "30-NOV-2012", "1400", "18:00", "C34", "280");
        dao.put(flight);
        flight = new Flight("AFR045", "Liverpool", "Madrid", "25-NOV-2012",
                "21:25", "23:45", "30-NOV-2012", "8:45", "12:05", "B9", "300");
        dao.put(flight);

        //Flight from Vienna to Rome
        dao.put(flight);
        flight = new Flight("KLM140", "Vienna", "Rome", "25-NOV-2012",
                "18:20", "21:20", "30-NOV-2012", "14:45", "18:05", "B5", "320");
        dao.put(flight);
        flight = new Flight("LHT2311", "Vienna", "Rome", "25-NOV-2012",
                "18:20", "21:20", "30-NOV-2012", "14:45", "18:05", "B5", "320");
        dao.put(flight);
        flight = new Flight("AFR5630", "Vienna", "Rome", "25-NOV-2012",
                "18:20", "21:20", "30-NOV-2012", "14:45", "18:05", "B5", "320");
        dao.put(flight);

        //Flight from Antalya to Zagreb
        flight = new Flight("KLM560", "Antalya", "Zagreb", "25-NOV-2012",
                "18:20", "21:20", "30-NOV-2012", "14:45", "18:05", "B5", "320");
        dao.put(flight);
        flight = new Flight("LHT12030", "Antalya", "Zagreb", "25-NOV-2012",
                "18:00", "21:20", "30-NOV-2012", "14:00", "18:00", "C34", "280");
        dao.put(flight);
        flight = new Flight("AFR2100", "Antalya", "Zagreb", "25-NOV-2012",
                "21:25", "23:45", "30-NOV-2012", "8:45", "12:05", "B9", "300");
        dao.put(flight);


        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Create DB</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1> DataBase succesuflly created!" + "</h1>");
            out.println("<h2> Go to <a href=\"index.jsp\"> Home page </a> " + "</h2>");
            out.println("</body>");
            out.println("</html>");

        } finally {
            out.close();
        }

    }
}
