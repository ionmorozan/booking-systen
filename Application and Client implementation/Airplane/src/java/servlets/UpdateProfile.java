package servlets;

import classes.DAO;
import classes.Profile;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Used when the user clicks Profile on the home page and wants to modify some
 * fields in his profile
 * @author Sandu Andreea & Morozan Ion
 */
public class UpdateProfile extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String street = request.getParameter("street");
        String city = request.getParameter("city");
        String department = request.getParameter("department");

        /* all fields must be filled */
        if (firstName.isEmpty() || lastName.isEmpty()
                || email.isEmpty() || phone.isEmpty()) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the fields! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
            rd.forward(request, response);
            /* wrong phone format*/
        } else if (!phone.matches("[0-9]+")) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Phone Format! Must contains only numbers </font>");
            RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
            rd.forward(request, response);
        } else {
            HttpSession session = request.getSession(false);
            String username=(String) session.getAttribute("username");
            if(username==null) 
            {
                request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must be logged in to update your profile! </font>");
                RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
                rd.forward(request, response);
            }
            else {

                DAO dao = DAO.getInstance();
                /*We find the corresponding user profile*/
                Profile profile = (Profile) dao.get(2, username);

                /*we modify the profile accordingly*/
                profile.setFirstName(firstName);
                profile.setLastName(lastName);
                profile.setEmail(email);
                profile.setPhone(phone);

                if (!street.isEmpty())
                    profile.setStreet(street);
                if (!city.isEmpty())
                    profile.setCity(city);
                if (!department.isEmpty())
                    profile.setDepartment(department);

                /*we overwrite the profile in the database*/
                dao.put(profile);

                request.setAttribute("errorMessage", "<font size=\"3\" "
                        + "face=\"arial\"color=\"green\">"
                        + "Your profile was succesfully updated!" + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("profile.jsp");
                rd.forward(request, response);
                response.sendRedirect("profile.jsp");
            }
        }
    }
}
