/**
 * The Search Availabilty Reservation Page
 * @author Morozan Ion & Sandu Andreea
 */
package servlets;

import classes.Availability;
import classes.DAO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.xml.sax.InputSource;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

public class SearchHotel extends HttpServlet {

    private String checkin;
    private String checkout;
    private String room_size;
    private ArrayList<String> rooms_id = new ArrayList<String>();
    private ArrayList<String> kind = new ArrayList<String>();
    private ArrayList<String> description = new ArrayList<String>();

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/WEB-INF/pay.jsp").forward(req, resp);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        checkin = request.getParameter("adate");
        checkout = request.getParameter("ddate");
        room_size = request.getParameter("room_size");

        /*current date*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();

        /* instance of database */
        DAO dao = DAO.getInstance();

        /* all fields are mandatory to search a room */
        if (checkin.isEmpty() || checkout.isEmpty() || room_size.isEmpty()) {
            /*otherwise error message is sent */
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all fields!");
            RequestDispatcher rd = request.getRequestDispatcher("pay.jsp");
            rd.forward(request, response);
            /* and of course the dates must be in a logical order */
        } else if ((checkin.compareTo(checkout) > 0)
                || (checkin.compareTo(dateFormat.format(date).toString()) <= 0)) {
            request.setAttribute("errorMessage", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Date! </font> ");
            RequestDispatcher rd = request.getRequestDispatcher("pay.jsp");
            rd.forward(request, response);
        } else {
            /* GET method */

            /* create the query link*/
            String link = "http://mdwhotel.appspot.com/api/v1/reservation/?start=";
            link += checkin + "&end=" + checkout + "&nr=" + room_size;

            Client client = Client.create();
            WebResource webResource = client.resource(link);
            webResource.accept("application/xml");
            /* invoke the specific metgod(GET) for getting the avialable rooms*/
            String resp = webResource.get(String.class);


            /*parsing the String to XML format and extract important fields*/
            StringToXML(resp);

            /* create new availability req*/
            Availability available = new Availability(checkin, checkout, room_size, rooms_id, kind, description);
            dao.put(available);

            response.sendRedirect("bookHotel.jsp");
        }
    }

    /**
     * Parse the xml file and extract the desired fields
     * @param xmlString = xml file receive from the remote application
     */
    protected void StringToXML(String xmlString) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder builder;
        try {
            /*clear arrays if they contain information from previous GET*/
            rooms_id.clear();
            kind.clear();
            description.clear();


            builder = factory.newDocumentBuilder();
            // Use String reader
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("room");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    rooms_id.add(getTagValue("id", eElement));
                    kind.add(getTagValue("kind_of_the_room", eElement));
                    description.add(getTagValue("description", eElement));
                }
            }
        } catch (Exception e) {
            System.out.println("Eroare la parsare XML ");
        }
    }

    /*
     * Extract the Tag and what contains
     */
    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }
}
