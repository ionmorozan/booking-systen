package servlets;

import classes.DAO;
import classes.User;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The user can check in online with his username, password and confirmation
 * number of the flight
 * @author Sandu Andreea & Morozan Ion
 */
public class CheckInOnline extends HttpServlet {

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String confNumber = request.getParameter("confNumber");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String userType = "User";

        /* instance of database */
        DAO dao = DAO.getInstance();

        /* get the confirmation number */
        int confirmNumber = 0;
        if (!confNumber.isEmpty()) confirmNumber = Integer.valueOf(confNumber);

        /* create new user */
        User user = new User(username, password, userType);


        /* all fields must be filled && pass must match */
        if (username.isEmpty() || password.isEmpty() || !dao.exists(user.id, username)
                /*extract the pass from the DB and compare with the one that
                 the user enters now*/
                || !password.equals(((User) dao.get(user.id, username)).password)) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid username or password. Please try again! <br/>"
                    + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("checkInOnline.jsp");
            rd.forward(request, response);
        } else if (!confNumber.matches("[0-9]*") ||
                (dao.getConfirmationNumber(username) != confirmNumber) ||
                (confirmNumber == 0)) {
            
            /* the confirmation number must contain only digits and it must match*/
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "The confirmation number is not correct! <br/>"
                    + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("checkInOnline.jsp");
            rd.forward(request, response);
        }
        else
            {
                request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"green\">"
                    + "You have successfully checked-in online! <br/>"
                    + "</font>");
                RequestDispatcher rd = request.getRequestDispatcher("checkInOnline.jsp");
                rd.forward(request, response);
                response.sendRedirect("checkInOnline.jsp");
            }
        }
}