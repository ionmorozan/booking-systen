package servlets;

import classes.*;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The admin can delete any type of account, except his own account
 * @author Sandu Andreea & Morozan Ion
 */
public class AdminDeleteAccount extends HttpServlet {


    /**
     * Handles the HTTP <code>GET</code> method.
     * @param request Servlet request
     * @param response Servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/company.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String usernameDel = request.getParameter("usernameDel");

        /* instance of database */
        DAO dao = DAO.getInstance();

        /* all fields must be filled && pass must match */
        if (usernameDel.isEmpty() || !dao.exists(1, usernameDel)) {
            request.setAttribute("errorMessage", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid username!<br/>"
                    + "</font>");
            RequestDispatcher rd = request.getRequestDispatcher("adminPanel.jsp");
            rd.forward(request, response);
        } else {
            User user = (User)dao.get(1, usernameDel);
            /* delete the user or the company with that username from the database */
            if (user.group.equals("User"))
                dao.delete(2, usernameDel);
            else
                dao.delete(4, usernameDel);
            dao.delete(1, usernameDel);

            request.setAttribute("accountDeleted", "<font size=\"2\" "
                    + "face=\"arial\" color=\"green\">"
                    + "The account has been deleted! </font>");
            RequestDispatcher rd = request.getRequestDispatcher("adminPanel.jsp");
            rd.forward(request, response);
            response.sendRedirect("adminPanel.jsp");
            }
        }

}
