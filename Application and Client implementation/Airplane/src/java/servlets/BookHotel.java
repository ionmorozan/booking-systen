/**
 * The Hotel Book Reservation Page
 * An Customer can make a reservation for a hotel room
 * @author Morozan Ion & Sandu Andreea
 */
package servlets;

import classes.DAO;
import classes.HotelReservation;
import java.io.IOException;
import java.util.regex.Pattern;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.io.StringReader;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

public class BookHotel extends HttpServlet {

    /* Those are the fields req for making a reservation
     * id of the reservation, name, email, checkin, checkout
     */
    private String Rid;
    private String Rstart;
    private String Rend;
    private String RroomId;
    private String RnrOfPers;
    private String Rdesc;
    private String Rname;
    private String Remail;

    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/WEB-INF/bookHotel.jsp").forward(req, resp);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String checkin = request.getParameter("adate");
        String checkout = request.getParameter("ddate");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String room_id = request.getParameter("rooms_id");


        /* instance of database */
        DAO dao = DAO.getInstance();


        /* test if the req fields are filled */
        if (name.isEmpty() || email.isEmpty()) {
            /* sent error  message if not */
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all fields!");
            RequestDispatcher rd = request.getRequestDispatcher("bookHotel.jsp");
            rd.forward(request, response);
            /* verify if the email address is in a correct format*/
        } else if (!rfc2822.matcher(email).matches()) {
            request.setAttribute("errorMessage", "<font size=\"3\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Invalid email address!");
            RequestDispatcher rd = request.getRequestDispatcher("bookHotel.jsp");
            rd.forward(request, response);
        } else {
            /* POST method */

            /*erase all the whiteSpaces*/
            String auxName = name;
            /* create the query link */
            String link = "http://mdwhotel.appspot.com/api/v1/reservation?start=";
            link += checkin + "&end=" + checkout + "&room_id=" + room_id
                    + "&username=" + auxName.replaceAll(" ", "") + "&email=" + email;

            Client client = Client.create();
            WebResource webResource = client.resource(link);
            webResource.accept("application/xml");
            String resp = webResource.post(String.class);

            /*parsing the String to XML format and extract important fields*/
            StringToXML(resp);

            /* getting the session information to find out the current user*/
            HttpSession session = request.getSession(false);
            String username = (String) session.getAttribute("username");
            String cityTo = (String) session.getAttribute("cityTo");

            /* if it's not the Guest user than the reservation made by an
             * autenthicate user will be saved in our local DB if there are
             * available rooms
             * !Guest customer cannot modify their reservations
             */
            if (username != null) {
                HotelReservation HR = new HotelReservation(Rid, Rstart, Rend,
                                                           RroomId, RnrOfPers, Rdesc, name, Remail, username, cityTo);
                dao.put(HR);
            }
            RequestDispatcher rd = request.getRequestDispatcher("reservationSuccessful.jsp");
            rd.forward(request, response);
            response.sendRedirect("reservationSuccessful.jsp");
        }

    }
    /* email verification */
    private static final Pattern rfc2822 = Pattern.compile(
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
            + "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");

    /**
     * Parse the xml file and extract the desired fields
     * @param xmlString = xml file receive from the remote application
     */
    protected void StringToXML(String xmlString) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {

            builder = factory.newDocumentBuilder();
            // Use String reader
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

            doc.getDocumentElement().normalize();
            NodeList nList = doc.getElementsByTagName("reservation");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    Rid = getTagValue("id", eElement);
                    Rstart = getTagValue("start", eElement);
                    Rend = getTagValue("end", eElement);
                    RroomId = getTagValue("roomId", eElement);
                    RnrOfPers = getTagValue("nrOfPersons", eElement);
                    Rdesc = getTagValue("description", eElement);
                    Rname = getTagValue("username", eElement);
                    Remail = getTagValue("email", eElement);
                }
            }
        } catch (Exception e) {
            System.out.println("Eroare la parsare XML ");
        }
    }

    /*
     * Extract the Tag and what contains
     */
    private static String getTagValue(String sTag, Element eElement) {
        NodeList nlList = eElement.getElementsByTagName(sTag).item(0).getChildNodes();

        Node nValue = (Node) nlList.item(0);

        return nValue.getNodeValue();
    }
}
