package servlets;

import classes.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.RequestDispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Used when searching for a plane ticket
 * @author Morozan Ion & Sandu Andreea
 */
public class Search extends HttpServlet {
    /**
     * Handles the HTTP <code>GET</code> method.
     * @param req servlet request
     * @param resp servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(req, resp);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String from = request.getParameter("from");
        String to = request.getParameter("to");
        String leaving = request.getParameter("leaving");
        String returning = request.getParameter("return");
        String passengers = request.getParameter("passengers");
        /* for error message on the same page*/
        String onSearchpage = request.getParameter("onSearchpage");
        String page = "index.jsp";

        /* create new user */
        Ticket flight = new Ticket(from, to, leaving, returning, passengers);
        /* instance of database */
        DAO dao = DAO.getInstance();

        /*current date*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        Ticket addFlight = new Ticket(from, to, leaving, returning, passengers);
        dao.put(addFlight);

        /* all fields must be filled */
        if (leaving.isEmpty() || returning.isEmpty()) {
            request.setAttribute("allert", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "You must fill all the fields! </font>");
            if (onSearchpage != null) {
                page = "search.jsp";
            }
            RequestDispatcher rd = request.getRequestDispatcher(page);
            rd.forward(request, response);
            /* dates must be in a proper order returning date > leaving date
            and the leaving day must be at least equal to current date*/
        } else if ((Date.parse(returning) < Date.parse(leaving))
                || (Date.parse(leaving) <= Date.parse(dateFormat.format(date)))) {
            request.setAttribute("allert", "<font size=\"2\" "
                    + "face=\"arial\" color=\"red\">"
                    + "Wrong Date! </font> ");
            if (onSearchpage != null) {
                page = "search.jsp";
            }
            RequestDispatcher rd = request.getRequestDispatcher(page);
            rd.forward(request, response);
        } else {

            RequestDispatcher rd = request.getRequestDispatcher("search.jsp");
            rd.forward(request, response);
        }

    }
}
