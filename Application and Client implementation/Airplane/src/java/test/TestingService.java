/**
 *
 * @author Sandu Andreea & Morozan Ion
 */
package test;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/test/")
public class TestingService {
    @Context
    private UriInfo uriInfo;

    public static void insertion_srt(int array[], int n){
        for (int i = 1; i < n; i++){
            int j = i;
            int B = array[i];
            while ((j > 0) && (array[j-1] > B)){
                array[j] = array[j-1];
                j--;
            }
            array[j] = B;
        }
    }

    /*Time consuming operation means sorting a 100000 elements vector
     with insertion sort*/
    private int[] timeConsumingOperation() {
        int [] array = new int [100000];
        for (int i = 0; i < 100000; i++)
            array[i] = (int) (Math.random() * 10000);

        insertion_srt(array, 100000);
        return array;
    }

    @GET
    @Path("/no")
    @Produces("text/plain")
    public String noCache() {
        int []result = timeConsumingOperation();
        return String.valueOf(result);
    }
}
