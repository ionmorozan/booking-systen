
/**
 *
 * @author Morozan Ion & Sandu Andreea
 */

package test;

import javax.persistence.Id;

public class Scalability {

    /**
     * The number of the reservation
     */
    @Id
    private Long id;

    /**
     * The total time (cpu time + api time)
     */
    private double totalSeconds;
    
    private Scalability() {
    }

    /**
     * Constructor with parameters
     * @param scalabilityId The id used in the database for this type of objects
     * @param totalSeconds The total time (cpu time + api time)
     */
    public Scalability(Long scalabilityId, double totalSeconds) {
        this.id = scalabilityId;
        this.totalSeconds = totalSeconds;
    }

}
