/**
 *
 * @author Andreea Sandu & Ion Morozan
 */

package test;

import classes.DAO;
import com.google.appengine.api.quota.*;
import java.io.*;
import java.math.BigDecimal;
import javax.servlet.*;

// 10, 20, 50, 100, 200, 300, 400, 500 de 5 ori fiecare => media +
// parerea noastra in privinta rezultatului
// fiecare punct de pe grafic reprezinta 10, 20, 50, 100...
// google spreadsheet cu toate valorile, nu doar media

public class LogFilter implements Filter {
    
    String sir = "";
    //int i = 0;
    long i = 1000;
    FilterConfig fconf = null;
    public void init(FilterConfig fc) throws ServletException {
        this.fconf = fc;
    }
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        QuotaService qs = QuotaServiceFactory.getQuotaService();

        if (qs.supports(QuotaService.DataType.CPU_TIME_IN_MEGACYCLES)) {
            long startAPI = qs.getApiTimeInMegaCycles();
            long startCPU = qs.getCpuTimeInMegaCycles();
            fc.doFilter(sr, sr1);
            long endCPU = qs.getCpuTimeInMegaCycles();
            long endAPI = qs.getApiTimeInMegaCycles();
            double cpuSeconds = qs.convertMegacyclesToCpuSeconds(endCPU - startCPU);
            double apiSeconds = qs.convertMegacyclesToCpuSeconds(endAPI - startAPI);
            System.out.println("Scalability: cpu = " + cpuSeconds + ", api = " + apiSeconds + ", sum = " + (cpuSeconds + apiSeconds));

            i = (long) (Math.random() * 100000 + Math.random() * 100);
            System.out.println("Random = " + i);

            double targetDBL = apiSeconds + cpuSeconds;

            int decimalPlace = 2;
            BigDecimal bd = new BigDecimal(targetDBL);
            bd = bd.setScale(decimalPlace,BigDecimal.ROUND_UP);

            Scalability time = new Scalability(i , bd.doubleValue());
            
            DAO dao = DAO.getInstance();
            dao.put(time);

        }else{
           long startCPU = System.currentTimeMillis();
           fc.doFilter(sr, sr1);
           long endCPU = System.currentTimeMillis();
           double cpuSeconds = (endCPU - startCPU)/1000;

           System.out.println(cpuSeconds);
        }
    }
    public void destroy() {
        this.fconf = null;
    }
}