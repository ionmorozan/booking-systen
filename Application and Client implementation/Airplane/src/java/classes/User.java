package classes;

import javax.persistence.Id;

/**
 * @author Sandu Andreea & Morozan Ion
 */
public class User {

    /**
     * The username the user chose at registration
     */
    public @Id String username;

    /**
     * The password the user chose at registration
     */
    public String password;

    /**
     * The group of the user: User, Company or Admin
     */
    public String group;
    
    /**
     * The id we use to find User type objects in the database, which is 1
     */
    public int id;

    /**
     * The confirmation Number received when buying a ticket
     */
    private int confNumber;

    /**
     * Constructor with no parameters
     */
    private User() {}

    /**
     * Constructor with parameters
     * @param username The username the user chose at registration
     * @param password The password the user chose at registration
     * @param group The group of the user: User, Company or Admin
     */
    public User(String username, String password, String group) {
        this.username = username;
        this.password = password;
        this.group = group;
        this.id = 1;
        this.confNumber = 0;
    }

    /**
     * @return the confNumber
     */
    public int getConfNumber() {
        return confNumber;
    }

    /**
     * @param confNumber the confNumber to set
     */
    public void setConfNumber(int confNumber) {
        this.confNumber = confNumber;
    }
}
