    package classes;

    import com.googlecode.objectify.Objectify;
    import com.googlecode.objectify.ObjectifyService;
    import com.googlecode.objectify.util.DAOBase;
    import com.googlecode.objectify.Query;
    import java.util.List;

    /**
     * @author Sandu Andreea & Morozan Ion
     */
    public class DAO extends DAOBase {

        static {
            ObjectifyService.register(User.class);
            ObjectifyService.register(Profile.class);
            ObjectifyService.register(Ticket.class);
            ObjectifyService.register(CompanyProfile.class);
            ObjectifyService.register(Flight.class);
            ObjectifyService.register(Reservation.class);
            ObjectifyService.register(Availability.class);
            ObjectifyService.register(HotelReservation.class);
            ObjectifyService.register(test.Scalability.class);
        }
        Objectify ofy;

        /**
         * Class constructor with no parameters
         */
        public DAO() {
        }

        /**
         * Gets an instance to the database
         * @return The database object
         */
        public static DAO getInstance() {
            DAO dao = new DAO();
            dao.ofy = ObjectifyService.begin();
            return dao;
        }

        /**
         * Add an object to the database
         * @param st Object that will be added to the database
         */
        public void put(Object st) {
            ofy.put(st);
        }

        /**
         * Delete one object from the database
         * @param cls   The class of the object that we want to delete
         * @param id    The key we use to find the object in the database
         */
        public void delete(int cls, String id) {
            switch (cls) {
                case 1: {
                    ofy.delete(User.class, id);
                    break;
                }
                case 2: {
                    ofy.delete(Profile.class, id);
                    break;
                }
                case 3: {
                    ofy.delete(Ticket.class, id);
                    break;
                }
                case 4: {
                    ofy.delete(CompanyProfile.class, id);
                    break;
                }
                case 5: {
                    ofy.delete(Flight.class, id);
                    break;
                }
                default: {
                    break;
                }
            }
        }

        /**
         * Find one object in the database
         * @param cls   The class of the object that we want to find
         * @param id    The key we use to find the object in the database
         * @return      The object if it exists in the database, null otherwise
         */
        public Object get(int cls, String id) {
            Object ob = null;
            switch (cls) {
                case 1: {
                    ob = ofy.get(User.class, id);
                    break;
                }
                case 2: {
                    ob = ofy.get(Profile.class, id);
                    break;
                }
                case 3: {
                    ob = ofy.get(Ticket.class, id);
                    break;
                }
                case 4: {
                    ob = ofy.get(CompanyProfile.class, id);
                    break;
                }
                case 5: {
                    ob = ofy.get(Flight.class, id);
                    break;
                }
                default: {
                    break;
                }
            }
            return ob;
        }

        /**
         * Find out if an object exists already in the database
         * @param cls   The class of the object that we want to find
         * @param id    The key we use to find the object in the database
         * @return      True if the object exists in the database, false otherwise
         */
        public boolean exists(int cls, String id) {
            switch (cls) {
                case 1: {
                    if (ofy.find(User.class, id) != null) {
                        return true;
                    }
                    break;
                }
                case 2: {
                    if (ofy.find(Profile.class, id) != null) {
                        return true;
                    }
                    break;
                }
                case 3: {
                    if (ofy.find(Ticket.class, id) != null) {
                        return true;
                    }
                    break;
                }
                case 4: {
                    if (ofy.find(CompanyProfile.class, id) != null) {
                        return true;
                    }
                    break;
                }
                case 5: {
                    if (ofy.find(Flight.class, id) != null) {
                        return true;
                    }
                    break;
                }

                default: {
                    break;
                }

            }

            return false;
        }

        /**
         * Function used to get the confirmation number of a user
         * @param username Username of the user
         * @return The confirmation number
         */
        public int getConfirmationNumber(String username) {
            User user = ofy.get(User.class, username);
            return user.getConfNumber();
        }

        /**
         * Function used to set the confirmation number of a user
         * @param username Username of the user
         * @param confirmation Confirmation Number
         */
        public void setConfirmationNumber(String username, int confirmation) {
            if (!username.equals("Guest")) {
                User user = ofy.get(User.class, username);
                user.setConfNumber(confirmation);
                ofy.put(user);
            }
        }

        /**
         * Get a list of all objects of type Flight
         * @return  A list with all the objects in the database from Flight class
         */
        public List<Flight> getAllFlights() {
            return ofy.query(Flight.class).list();
        }

        /**
         * Get a list of all objects of type Ticket
         * @return  A list with all the objects in the database from Ticket class
         */
        public List<Ticket> getTicket() {
            return ofy.query(Ticket.class).list();
        }

        /* save a reservation afer an update*/
        public void saveReservation(Reservation t) {
            ofy.put(t);
        }

        /* delete a REservation from DB*/
        public void deleteReservation(Reservation t) {
            ofy.delete(t);
        }

        /*add a reservation to DB*/
        public void addReservation(Reservation t) {
            ofy.put(t);
        }
        /*return if the reservation exists or not in the DB*/

        public boolean existReservation(Long ReservationId) {

            if (ofy.find(Reservation.class, ReservationId) != null) {
                return true;
            }
            return false;
        }
        /*return a flight from DB using the id of the fliht I mean the fligt number
         * otherwise returns null
         */

        public Flight getFlightById(String id) {

            Query<Flight> result = ofy.query(Flight.class).filter("flightNumber", id);
            if (!result.list().isEmpty()) {
                return result.list().get(0);
            }
            return null;
        }

        /*Get all the tickets from DB with the disred tags
         */
        public List<Flight> getFlightsFrom(String from) {

            Query<Flight> result = ofy.query(Flight.class).filter("flightFrom", from);
            return result.list();
        }

        /*Get all the tickets from DB with the disred tags
         */
        public List<Flight> getFlightsFromTo(String from, String to) {

            Query<Flight> result = ofy.query(Flight.class).
                    filter("flightFrom", from).
                    filter("flightTo", to);
            return result.list();
        }

        /*Get all the tickets from DB with the disred tags
         */
        public List<Flight> getFlightsFromToDepartureReturn(String from, String to,
                                                            String departuring, String returning) {

            Query<Flight> result = ofy.query(Flight.class).
                    filter("flightFrom", from).
                    filter("flightTo", to).
                    filter("departureDate", departuring).
                    filter("returnDate", returning);
            return result.list();
        }

         public List<HotelReservation> getHotelReservation(String username) {

            Query<HotelReservation> result = ofy.query(HotelReservation.class).
                    filter("username", username);
            return result.list();
        }
    }
