/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package classes;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Morozan Ion & Sandu Andreea
 */
@XmlRootElement(name = "reservation")
public class Reservation {

    /**
     * The number of the reservation
     */
    @Id
    private Long id;
    /**
     * Name of the person
     */
    private String name;
    /**
     * The number of the flight
     */
    private String flightNumber;
    /**
     * The city of departure
     */
    private String from;
    /**
     * The city where the user wants to arrive
     */
    private String to;
    /**
     * The date of the departure
     */
    private String leave;
    /**
     * The date of the returning trip
     */
    private String returning;
    /**
     * Number of passengers
     */
    private String passengers;

        /**
     * Constructor with no parameters
     */
    private Reservation() {
    }

    /**
     * Constructor with parameters
     * @param reservationNumber The number of the reservation
     * @param name The name of the person who did the reservation
     * @param flightNumber The number of the flight
     * @param from The city of departure
     * @param to The city where the user wants to arrive
     * @param leave The date of the departure
     * @param returning The date of the returning trip
     * @param passengers Number of passengers
     */
    public Reservation(Long reservationNumber, String name, String flightNumber,
            String from, String to, String leave, String returning,
            String passengers) {
        this.id = reservationNumber;
        this.name = name;
        this.flightNumber = flightNumber;
        this.from = from;
        this.to = to;
        this.leave = leave;
        this.returning = returning;
        this.passengers = passengers;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the flightNumber
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @return the leave
     */
    public String getLeave() {
        return leave;
    }

    /**
     * @return the returning
     */
    public String getReturning() {
        return returning;
    }

    /**
     * @return the passengers
     */
    public String getPassengers() {
        return passengers;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param flightNumber the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @param leave the leave to set
     */
    public void setLeave(String leave) {
        this.leave = leave;
    }

    /**
     * @param returning the returning to set
     */
    public void setReturning(String returning) {
        this.returning = returning;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

}
