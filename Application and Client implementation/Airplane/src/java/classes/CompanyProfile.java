package classes;
import javax.persistence.Id;

/**
 * @author Sandu Andreea & Morozan Ion
 */

public class CompanyProfile {

    @Id
    String username;
    String password;
    String name;
    String regNumber;
    String regAuth;
    String email;
    String phone;
    String street;
    String city;
    String department;
    /**
     * The id we use to find CompanyProfile type objects in the database,
     * which is 4
     */
    public int id;

    /**
     * Class constructor with no parameters
     */
    private CompanyProfile() {
    }

    /**
     * Class constructor with parameters
     * 
     * @param username  The username chosen by the company during registration
     * @param password  The password chosen by the company during registration
     * @param name      The name of the company
     * @param regNumber The registration number of the company
     * @param regAuth   The registration authority of the company
     * @param email     The email of the company
     * @param phone     The phone of the company
     * @param street    The street where the company is situated
     * @param city      The city in which the company is situated
     * @param department    The department of the country where the company is
     */
    public CompanyProfile(String username, String password, String name,
            String regNumber, String regAuth, String email,
            String phone, String street, String city, String department) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.regNumber = regNumber;
        this.regAuth = regAuth;
        this.email = email;
        this.phone = phone;
        this.street = street;
        this.city = city;
        this.department = department;
        this.id = 4;
    }
}