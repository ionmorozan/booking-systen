package classes;

import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Morozan Ion & Sandu Andreea
 */
@XmlRootElement(name = "flight")
public class Flight {

    /**
     * The flight number
     */
    @Id
    private String flightNumber;
    /**
     * The city from where the flight departs
     */
    private String flightFrom;
    /**
     * The city where the flight arrives
     */
    private String flightTo;
    /**
     * The date when the flight departures
     */
    private String departureDate;
    /**
     * The time when the flight arrives at the destination
     */
    private String departureHour;
    /**
     * The time when the flight arrives at the destination
     */
    private String arrivalHour;
    /**
     * The date when the flight returns
     */
    private String returnDate;
    /**
     * The time when the flight departures
     */
    private String returnDepartureHour;
    /**
     * The time when the flight arrives at the destination
     */
    private String returnArrivalHour;
    /**
     * The gate coresponding to this flight
     */
    private String gate;
    /**
     * The price of the ticket
     */
    private String price;
    /**
     * The id we use to find Flight type objects in the database, which is 5
     */
    private Integer id;

    /**
     * Class constructor with no parameters
     */
    private Flight() {
    }

    /**
     * Class constructor with parameters
     * @param flightNumber The flight number
     * @param flightFrom The city from where the flight departs
     * @param flightTo The city where the flight arrives
     * @param departureDate The date when the flight departures
     * @param departureHour The time when the flight departures
     * @param arrivalHour The time when the flight arrives at the destination
     * @param returnDate The date when the flight returns
     * @param returnDepartureHour The time when the flight departures
     * @param returnArrivalHour The time when the flight arrives at the destination
     * @param gate The gate coresponding to this flight
     * @param price The price of the ticket
     */
    public Flight(String flightNumber, String flightFrom, String flightTo,
                  String departureDate, String departureHour, String arrivalHour,
                  String returnDate, String returnDepartureHour, String returnArrivalHour,
                  String gate, String price) {
        this.flightNumber = flightNumber;
        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
        this.departureDate = departureDate;
        this.departureHour = departureHour;
        this.arrivalHour = arrivalHour;
        this.returnDate = returnDate;
        this.returnDepartureHour = returnDepartureHour;
        this.returnArrivalHour = returnArrivalHour;
        this.gate = gate;
        this.price = price;
        this.id = 5;
    }

    /**
     * @return the flightNumber
     */
    @XmlElement
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * @param flightNumber the flightNumber to set
     */
    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    /**
     * @return the flightFrom
     */
    @XmlElement
    public String getFlightFrom() {
        return flightFrom;
    }

    /**
     * @param flightFrom the flightFrom to set
     */
    public void setFlightFrom(String flightFrom) {
        this.flightFrom = flightFrom;
    }

    /**
     * @return the flightTo
     */
    @XmlElement
    public String getFlightTo() {
        return flightTo;
    }

    /**
     * @param flightTo the flightTo to set
     */
    public void setFlightTo(String flightTo) {
        this.flightTo = flightTo;
    }

    /**
     * @return the departureDate
     */
    @XmlElement
    public String getDepartureDate() {
        return departureDate;
    }

    /**
     * @param departureDate the departureDate to set
     */
    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    /**
     * @return the returnDate
     */
    @XmlElement
    public String getReturnDate() {
        return returnDate;
    }

    /**
     * @param returnDate the returnDate to set
     */
    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    /**
     * @return the departureHour
     */
    @XmlElement

    public String getDepartureHour() {
        return departureHour;
    }

    /**
     * @param departureHour the departureHour to set
     */
    public void setDepartureHour(String departureHour) {
        this.departureHour = departureHour;
    }

    /**
     * @return the arrivalHour
     */
    @XmlElement

    public String getArrivalHour() {
        return arrivalHour;
    }

    /**
     * @param arrivalHour the arrivalHour to set
     */
    public void setArrivalHour(String arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    /**
     * @return the returnDepartureHour
     */
    @XmlElement

    public String getReturnDepartureHour() {
        return returnDepartureHour;
    }

    /**
     * @param returnDepartureHour the returnDepartureHour to set
     */
    public void setReturnDepartureHour(String returnDepartureHour) {
        this.returnDepartureHour = returnDepartureHour;
    }

    /**
     * @return the returnArrivalHour
     */
    @XmlElement

    public String getReturnArrivalHour() {
        return returnArrivalHour;
    }

    /**
     * @param returnArrivalHour the returnArrivalHour to set
     */
    public void setReturnArrivalHour(String returnArrivalHour) {
        this.returnArrivalHour = returnArrivalHour;
    }

    /**
     * @return the gate
     */
    @XmlElement

    public String getGate() {
        return gate;
    }

    /**
     * @param gate the gate to set
     */
    public void setGate(String gate) {
        this.gate = gate;
    }

    /**
     * @return the price
     */
    @XmlElement

    public String getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * @return the id
     */
    @XmlTransient
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
