package classes;

import javax.persistence.Id;

/**
 * @author Morozan Ion & Sandu Andreea
 */
public class Profile {

    @Id
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String street;
    private String city;
    private String department;
    /**
     * The id we use to find Profile type objects in the database, which is 2
     */
    private int id;

    /**
     * Class constructor with no parameters
     */
    public Profile() {
    }

    /**
     * Class constructor with parameters
     * @param username The username chosen by the user during registration
     * @param password The password chosen by the company during registration
     * @param firstName The first name of the user
     * @param lastName The last name of the user
     * @param email The email of the user
     * @param phone The phone number of the user
     * @param street The street where the user lives
     * @param city The city where the user lives
     * @param department The department of the country where the user lives
     */
    public Profile(String username, String password, String firstName,
            String lastName, String email, String phone, String street,
            String city, String department) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.street = street;
        this.city = city;
        this.department = department;
        this.id = 2;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the fistName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param fistName the fistName to set
     */
    public void setFirstName(String fistName) {
        this.firstName = fistName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

}
