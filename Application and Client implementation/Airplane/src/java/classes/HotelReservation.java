/**
 * Class who contains the details of a reservation made by a Customer
 * @author Ion Morozan & Andreea Sandu
 */

package classes;

import javax.persistence.Id;

public class HotelReservation {

    @Id
    private String id;
    private String start;
    private String end;
    private String roomId;
    private String nrOfPers;
    private String desc;
    private String name;
    private String email;
    private String city;
    /*
     * Username account of the buyer
     * (only registered users can EDIT/DELETE a reservation)
     */
    private String username;

    private HotelReservation() {
    }

    /**
     * Constructor with parameters
     */
    public HotelReservation(String id, String start, String end, String roomID,
                            String nrOfPers, String desc, String name,
                            String email, String username, String city) {
        this.id = id;
        this.start = start;
        this.end = end;
        this.roomId = roomID;
        this.nrOfPers = nrOfPers;
        this.desc = desc;
        this.name = name;
        this.email = email;
        this.username = username;
        this.city = city;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * @return the end
     */
    public String getEnd() {
        return end;
    }

    /**
     * @return the roomId
     */
    public String getRoomId() {
        return roomId;
    }

    /**
     * @return the nrOfPers
     */
    public String getNrOfPers() {
        return nrOfPers;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param start the start to set
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param roomId the roomId to set
     */
    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    /**
     * @param nrOfPers the nrOfPers to set
     */
    public void setNrOfPers(String nrOfPers) {
        this.nrOfPers = nrOfPers;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
