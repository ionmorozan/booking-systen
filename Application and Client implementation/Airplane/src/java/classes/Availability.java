/**
 * The class contains the details of a the available rooms that
 * the remote app has.
 * @author Ion Morozan and Andreea Sandu
 */
package classes;

import java.util.ArrayList;
import javax.persistence.Id;


public class Availability {

    /**
     * The date of checkin
     */
    @Id
    private String checkin;
    /**
     * The date of checkout
     */
    private String checkout;
    /**
     * The size of the room(how many persons
     */
    private String room_size;
    /**
     * Response from the server rooms id(res of the req)
     */
    private ArrayList<String> rooms_id;

    /**
     * Response from the server kind of the room(res of the req)
     */
    private ArrayList<String> kind;

    /**
     * Response from the server description of the room(res of the req)
     */
    private ArrayList<String> description;
    
    public int id;

    /**
     * Constructor with no parameters
     */
    private Availability() {
    }

    public Availability(String checkin, String checkout, String room_size,
                        ArrayList<String> rooms_id, ArrayList<String> kind,
                        ArrayList<String> description) {
        this.checkin = checkin;
        this.checkout = checkout;
        this.room_size = room_size;
        this.rooms_id = rooms_id;
        this.kind = kind;
        this.description = description;
        this.id = 6;
    }

    /**
     * @return the checkin
     */
    public String getCheckin() {
        return checkin;
    }

    /**
     * @return the checkout
     */
    public String getCheckout() {
        return checkout;
    }

    /**
     * @return the room_size
     */
    public String getRoom_size() {
        return room_size;
    }

    /**
     * @return the rooms_id
     */
    public ArrayList<String> getRooms_id() {
        return rooms_id;
    }

    /**
     * @return the kind
     */
    public ArrayList<String> getKind() {
        return kind;
    }

    /**
     * @return the description
     */
    public ArrayList<String> getDescription() {
        return description;
    }

  
}
