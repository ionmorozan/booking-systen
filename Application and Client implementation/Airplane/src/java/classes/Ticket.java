package classes;

import javax.persistence.Id;

/**
 * @author Morozan Ion & Sandu Andreea
 */
public class Ticket {

    /**
     * The city of departure
     */
    @Id
    private String from;
    /**
     * The city where the user wants to arrive
     */
    private String to;
    /**
     * The date of the departure
     */
    private String leave;
    /**
     * The date of the returning trip
     */
    private String returning;
    /**
     * Number of passengers
     */
    private String passengers;
    /**
     * The id we use to find Ticket type objects in the database, which is 3
     */
    public int id;

    /**
     * Constructor with no parameters
     */
    private Ticket() {
    }

    /**
     * Constructor with parameters
     * @param from The city of departure
     * @param to The city where the user wants to arrive
     * @param leave The date of the departure
     * @param returning The date of the returning trip
     * @param passengers Number of passengers
     */
    public Ticket(String from, String to, String leave, String returning,
            String passengers) {
        this.from = from;
        this.to = to;
        this.leave = leave;
        this.returning = returning;
        this.passengers = passengers;
        this.id = 3;
    }

    /**
     * @return the from
     */
    public String getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(String from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public String getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(String to) {
        this.to = to;
    }

    /**
     * @return the leave
     */
    public String getLeave() {
        return leave;
    }

    /**
     * @param leave the leave to set
     */
    public void setLeave(String leave) {
        this.leave = leave;
    }

    /**
     * @return the returning
     */
    public String getReturning() {
        return returning;
    }

    /**
     * @param returning the returning to set
     */
    public void setReturning(String returning) {
        this.returning = returning;
    }

    /**
     * @return the passengers
     */
    public String getPassengers() {
        return passengers;
    }

    /**
     * @param passengers the passengers to set
     */
    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }
}
