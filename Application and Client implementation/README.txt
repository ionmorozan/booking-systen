Team:  Morozan Ion - morozion@fel.cvut.cz
	   Sandu Andreea - sanduand@fel.cvut.cz
	   
Class: MI-MDW
Milestone:	 2

			Application and client implementation

1.Link of the project on-line:

	-> fit-mdw-ws11-201-4.appspot.com
	
2. Svn repository:
	
	-> https://project.fit.cvut.cz/svn/Mdw11Ws201tm4
	
3.Develop details:
	
	-> The project is made using Html and Java Servlets(JSP) 
	   and a little bit of Java Script.
	-> For hosting and Database storage we used the Google Application
       Engine provided by Google
	-> The project uses for local host running 8080 port.
	-> The main page is "index.jsp"
	-> For more details about the features you can 
	   check de document assigned to the first milestone.

	   
4.Testing(Servlets Test.java):

	-> To create a Database for testing you can press the 
	   button on the bottom of the page("index.jsp"), "Create DataBase!"
	-> The button "Create DataBase!" generate some users, an admin user,
       company users,profiles and add some filgths.
	-> the main password for users/company users is: test
							 admin is :				 admin
	-> I added some flights but only with 2 dates:
			departure: 25-NOV-11
			arrival:   30-NOV-11
	-> The Flights are:
			Barcelona - Paris
			Antalya - Zagreb
			Liverpool - Madrid
			Vienna - Rome
			
4.Notes:
	
	-> Operating System used for develop: Windows 7
	-> IDE: NetBeans 6.9.1
	-> Difficutly: Average(> 35 hours of work)
	